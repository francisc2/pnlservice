﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PnLService.ServiceInterface
{
    /// <summary>
    /// Version 1.99
    /// </summary>
    public class AccountRecord
    {
        public string Type { get; set; }
        public string AccountID { get; set; }
        public string AccountTitle { get; set; }
        public string Street { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string CustomerType { get; set; }
        public string AccountType { get; set; }
        public string BaseCurrency { get; set; }
        public string MasterAccountID { get; set; }
        public string Van { get; set; }
        public string Capabilities { get; set; }
        public string Alias { get; set; }
        public string PrimaryEmail { get; set; }
        public string DateOpened { get; set; }
        public string DateClosed { get; set; }
        public string DateFunded { get; set; }
        public string AccountRepresentative { get; set; }
    }
}
