﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PnLService.ServiceInterface
{
    public enum FileType
    {
        Account,
        Activity,
        CashReport,
        NAV,
        PnL,
        Position,
        Security,
        Unknown
    }
}
