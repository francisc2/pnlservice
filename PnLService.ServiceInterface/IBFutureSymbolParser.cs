﻿using System;
using ServiceStack;

namespace PnLService.ServiceInterface
{
    internal class IBFutureSymbolParser
    {
        static char[] delimiterCharArray = new char[] { ' ' };
        public string SymbolRoot { get; private set; }
        public string MaturityMonthYear { get; private set; }

        public IBFutureSymbolParser(string ibSymbol)
        {
            //Symbols can be in various formats:
            //a) NQU3
            //b) ZF   SEP 23
			//c) FGBX 20230907 M
            //What else? fuuuuck.

            //1. if space, parse as b) and c)
            //2. else, parse as strandard full exchange symbol
            //3. log anything that is weird.

            string[] s = ibSymbol.Split(delimiterCharArray, StringSplitOptions.RemoveEmptyEntries);

			if (s.Length == 1)
			{
				//MNQU3
				//Maybe in some cases it will be MNQU23 ? without a space we can't parse this easily.
				var shortmonthyear = ibSymbol.Substring(ibSymbol.Length - 2);
				MaturityMonthYear = InstrumentUtils.Instrument.GetMaturityMonthYear(shortmonthyear);
				SymbolRoot = ibSymbol.Substring(0, ibSymbol.Length - shortmonthyear.Length);
			}
			else
			{
				//Could be "ZF   SEP 23" or "FGBX 20230907 M". What could be more fun? <vomit>
				//So for "ZF   SEP 23", we have s[0]=ZF, s[1]="SEP" and s[2]="23"
				//And for "FGBX 20230907 M" we have s[0]="FGBX", s[1]="20230907" and s[2]="M"
				
				//This part never changes
				SymbolRoot = s[0].Trim();

				if (s[1].Length == 3)
				{
					//"SEP"
					MaturityMonthYear = ParseMonthYear(s[1].Trim() + s[2].Trim());
				}
				else if (s[1].Length == 8)
				{
					//"20230907"
					MaturityMonthYear = s[1].Trim();
				}
				else
				{
					throw new Exception($"IBFutureSymbolParser(): Unsupported Futures symbol format: {ibSymbol}");
				}
			}
        }

        /// <summary>
		/// Parse expiry sent as "SEP23".
		/// Returns a FIX formatted MaturityMonthYear as yyyyMM
		/// </summary>
		/// <param name="monthYear">Must be in MMMyy format e.g. DEC23</param>
		/// <returns></returns>
		private static string ParseMonthYear(string monthYear)
		{
			if (monthYear.Length != 5)
			{
				throw new Exception($"IBFutureSymbolParser.ParseMonthYear: Invalid monthYear lenght: {monthYear}");
			}

			string year = $"20{monthYear.Substring(3)}";
			string month = monthYear.ToUpperInvariant().Substring(0, 3);
			switch (month)
			{
				case "JAN":
					return year + "01";
				case "FEB":
					return year + "02";
				case "MAR":
					return year + "03";
				case "APR":
					return year + "04";
				case "MAY":
					return year + "05";
				case "JUN":
					return year + "06";
				case "JUL":
					return year + "07";
				case "AUG":
					return year + "08";
				case "SEP":
					return year + "09";
				case "OCT":
					return year + "10";
				case "NOV":
					return year + "11";
				case "DEC":
					return year + "12";
				default:
					return monthYear;
			}
		}
    }
}