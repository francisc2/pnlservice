﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PnLService.ServiceInterface
{
    /// <summary>
    /// Version 1.99
    /// Type,AccountID,ConID,SecurityID,Symbol,BBTicker,BBGlobalID,SecurityDescription,AssetType,Currency,BaseCurrency,TradeDate,TradeTime,SettleDate,OrderTime,TransactionType,Quantity,UnitPrice,GrossAmount,SECFee,Commission,Tax,Net,NetInBase,TradeID,TaxBasisElection,Description,FxRateToBase,ContraPartyName,ClrFirmID,Exchange,MasterAccountID,Van,AwayBrokerCommission,OrderID,ClientReference,TransactionID,ExecutionID,CostBasis,Flag,
    /// </summary>
    public class ActivityRecord : LogBase
    {
        /// <summary>
        /// D
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// Account number
        /// </summary>
        public string AccountID { get; set; }
        /// <summary>
        /// Internal unique identifier for all asset types. 
        /// Full asset information can be found in the Security File Layout.
        /// Empty for Asset Type CASH.
        /// </summary>
        public string ConID { get; set; }
        /// <summary>
        /// ISIN for US and nonUS securities
        /// </summary>
        public string SecurityID { get; set; }
        /// <summary>
        /// Empty for Asset Type CASH
        /// </summary>
        public string Symbol { get; set; }
        /// <summary>
        /// Bloomberg ticker
        /// </summary>
        public string BBTicker = null;
        /// <summary>
        /// Bloomberg Global ID
        /// </summary>
        public string BBGlobalID = null;
        /// <summary>
        /// Description of the security
        /// </summary>
        public string SecurityDescription = null;

        public string AssetType { get; set; }
        /// <summary>
        /// Currency of the asset
        /// </summary>
        public string Currency { get; set; }
        /// <summary>
        /// Base currency of the account
        /// </summary>
        public string BaseCurrency { get; set; }
        /// <summary>
        /// CCYYMMDD (date format configurable)
        /// </summary>
        public string TradeDate { get; set; }
        /// <summary>
        /// Time of the trade, in HH:mm:ss format
        /// </summary>
        public string TradeTime { get; set; }
        /// <summary>
        /// CCYYMMDD (date format configurable)
        /// </summary>
        public string SettleDate { get; set; }

        public string OrderTime = null;
        /// <summary>
        /// For more information, see Transaction Type.
        /// </summary>
        public string TransactionType { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? UnitPrice { get; set; }
        /// <summary>
        /// -Quantity * Price
        /// Note that this is a general formula and does not consider multipliers or notional price (futures), bond pricing (/100), etc.
        /// </summary>
        public decimal? GrossAmount { get; set; }
        public decimal? SECFee { get; set; }
        /// <summary>
        /// Negative amount indicates commission charged.
        /// Positive amount indicates commission rebate. 
        /// For Forex trades, the commission is reported in base currency.
        /// </summary>
        public decimal? Commission { get; set; }
        public decimal? Tax { get; set; }
        /// <summary>
        /// Total cash received or delivered.
        /// Not included on Forex trades.
        /// For Futures and CFDs, this is the transaction MTM.
        /// </summary>
        public decimal? Net { get; set; }
        /// <summary>
        /// Total cash received or delivered.
        /// For Futures and CFDs, this is the transaction MTM.
        /// </summary>
        public decimal? NetInBase { get; set; }
        /// <summary>
        /// Trade ID, unless cancel or correction, then it is the original Trade ID
        /// </summary>
        public string TradeID { get; set; }
        /// <summary>
        /// For trades, represents the tax basis election
        /// </summary>
        public string TaxBasisElection { get; set; }
        /// <summary>
        /// Activity description
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Conversion rate from asset currency to base currency.
        /// </summary>
        public decimal? FXRatetoBase = null;
        /// <summary>
        /// For DVP records that represent the contra broker.
        /// </summary>
        public string ContraPartyName = null;
        /// <summary>
        /// For DVP records, the contra broker’s ID.
        /// </summary>
        public string ClrFirmID = null;
        /// <summary>
        /// Included for trades.
        /// </summary>
        public string Exchange = null;
        /// <summary>
        /// The Account ID of the master account.
        /// </summary>
        public string MasterAccountID = null;
        /// <summary>
        /// Virtual Account Number
        /// </summary>
        public string Van = null;
        /// <summary>
        /// The away broker's commission charge.
        /// </summary>
        public decimal? AwayBrokerCommission = null;
        /// <summary>
        /// The IB Order ID.
        /// </summary>
        public string OrderID = null;
        /// <summary>
        /// Client note entered by cashiering.
        /// </summary>
        public string ClientReference = null;
        /// <summary>
        /// The IB TransactionID.
        /// </summary>
        public string TransactionID = null;
        /// <summary>
        /// The IB ExecutionID.
        /// </summary>
        public string ExecutionID = null;
        /// <summary>
        /// The basis of an opening trade is the inverse of proceeds plus commission and tax amount. 
        /// For closing trades, the basis is the basis of the opening trade.
        /// </summary>
        public string CostBasis = null;
        /// <summary>
        /// For more information, see Flag Codes
        /// </summary>
        public string Flag = null;
    }
}
