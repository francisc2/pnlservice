﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PnLService.ServiceInterface
{
    public class NAVRecord
    {
        public string Type { get; set; }
        public string AccountID { get; set; }
        public string BaseCurrency { get; set; }
        public decimal Cash { get; set; }
        public decimal CashCollateral { get; set; }
        public decimal Stocks { get; set; }
        public decimal SecuritiesBorrowed { get; set; }
        public decimal SecuritiesLent { get; set; }
        public decimal Options { get; set; }
        public decimal Bonds { get; set; }
        public decimal Commodities { get; set; }
        public decimal Funds { get; set; }
        public decimal Notes { get; set; }
        public decimal Accruals { get; set; }
        public decimal DividendAccruals { get; set; }
        public decimal SoftDollars { get; set; }
        //public decimal InsuredBankDepositSweepProgram { get; set; }
        //public decimal InsuredBankDepositSweepProgramIntAccruals { get; set; }
        public decimal Totals { get; set; }
        public decimal TWR { get; set; }
        public decimal CFDUnrealizedPL { get; set; }
        //public decimal ForexUnrealizedPL { get; set; }
        //public decimal IPOSubscription { get; set; }
    }
}
