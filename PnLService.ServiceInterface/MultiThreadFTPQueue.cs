﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PgpCore;
using System.IO;
using System.Net;
using ServiceStack;
using FluentFTP;
using System.Threading;
using System.Globalization;
using System.Diagnostics;
using Database.BrokerAccountInfo.Entities;

namespace PnLService.ServiceInterface
{
    public class MultiThreadFTPQueue
    {
        static readonly NLog.Logger logger = NLog.LogManager.GetLogger("MultiThreadFTPQueue");
        BlockingCollection<FtpListItem> jobs;
        int numFiles;
        volatile int filesDownloaded;
        CancellationToken token;
        DirectoryInfo archiveDirectoryInfo, rawDirectoryInfo;
        BrokerCode brokerCode;

        public MultiThreadFTPQueue(CancellationToken token, BrokerCode brokerCode)
        {
            this.token = token;
            this.brokerCode = brokerCode;

            //Make sure local paths exist
            rawDirectoryInfo = Directory.CreateDirectory($"{Properties.Settings.Default.LocalFolder}\\{brokerCode.Name}\\Raw");
            archiveDirectoryInfo = Directory.CreateDirectory($"{Properties.Settings.Default.LocalFolder}\\{brokerCode.Name}\\UnprocessedArchive");           
        }


        /// <summary>
        /// Download all files in the FTP and delete them from FTP server
        /// This will be called on a timer every day at 9:15am.
        /// We can also call it at any time manually though a web request.
        /// </summary>
        public void Run()
        {
            //Download list of files            
            using (FtpClient client = new FtpClient(brokerCode.Host, brokerCode.Username, brokerCode.Password))
            {
                client.Connect();
                jobs = new BlockingCollection<FtpListItem>(new ConcurrentQueue<FtpListItem>(client.GetListing("/outgoing", FtpListOption.Auto)));
                numFiles = jobs.Count();
                client.Disconnect();

                if (numFiles == 0)
                {
                    logger.Info($"No new files exist on {brokerCode.Name} FTP server");
                    return;
                }
                else
                {
                    logger.Info($"Downloading {numFiles} {brokerCode.Name} files");
                }
            }

            //This will cause 'jobs.GetConsumingEnumerable' to stop blocking and exit when it's empty
            jobs.CompleteAdding();

            //Create up to MaxFTPClients for better throughput. At least 100 files per client
            int maxClients = Math.Min(Math.Max(1, numFiles / 100), Properties.Settings.Default.MaxFTPClients);
            // Semaphore semaphore = new Semaphore(1, maxClients);
            logger.Debug($"Spinning up {maxClients} FTP clients for {brokerCode.Name}");
            //var parallelOptions = new ParallelOptions();
            //parallelOptions.MaxDegreeOfParallelism = maxClients;
            //parallelOptions.CancellationToken = token;
            //Parallel.ForEach(files, parallelOptions, f => Process(f, semaphore));

            //privateKeyStream = new FileStream(Properties.Settings.Default.PGPKeyFullPath, FileMode.Open);

            Stopwatch sw = Stopwatch.StartNew();

            List<Task> tasks = new List<Task>();
            for (int i = 0; i < maxClients; i++)
            {
                tasks.Add(Task.Factory.StartNew(FTPClientThreadStart, token, TaskCreationOptions.LongRunning, TaskScheduler.Default));
            }

            //Wait until we're finished. This could take a while. Max 30 minutes.
            Task.WaitAll(tasks.ToArray(), Properties.Settings.Default.DownloadTimeoutMS, token);

            sw.Stop();
            logger.Info($"Downloading {filesDownloaded}/{numFiles} files for {brokerCode.Name} took {Utils.FormatTime(sw)}");
        }

        /// <summary>
        /// Launch a FTP client and download files from the IB FTP
        /// </summary>
        private void FTPClientThreadStart()
        {
            string clientId = "undefined";
            try
            {
                using (FtpClient client = new FtpClient(brokerCode.Host, brokerCode.Username, brokerCode.Password))
                {
                    //Progress<FtpProgress> progress = new Progress<FtpProgress>(x =>
                    //                {

                    //                    logger.Info($"({x.TransferSpeedToString()}) {x.Progress}% complete. ETA is {x.ETA.TotalSeconds}");
                    //                });

                    client.Connect();
                    clientId = client.GetId().ToString();
                    string localFileFullPath;
                    logger.Info($"FTP Client {clientId} is Connected");

                    foreach (FtpListItem job in jobs.GetConsumingEnumerable(token))
                    {
                        //Download files we won't process to a subfolder for safekeeping
                        FileType fileType = Utils.GetFileType(job.Name);
                        if (fileType != FileType.Account
                            && fileType != FileType.NAV 
                            && fileType != FileType.CashReport
                            && fileType != FileType.Security
                            && fileType != FileType.Activity)
                        {
                            localFileFullPath = $"{archiveDirectoryInfo.FullName}\\{job.Name}";
                        }
                        else
                        {
                            localFileFullPath = $"{rawDirectoryInfo.FullName}\\{job.Name}";
                        }

                        try
                        {
                            if (client.DownloadFile(localFileFullPath, job.FullName, FtpLocalExists.Skip, FtpVerify.Retry|FtpVerify.Delete, null) == FtpStatus.Success)
                            {
                                //Delete the FTP file if in PROD
                                if (false == System.Diagnostics.Debugger.IsAttached)
                                    client.DeleteFile(job.FullName);

                                logger.Debug($"{++filesDownloaded}/{numFiles} files downloaded");
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.Warn(ex, $"DownloadFile error for ClientID={clientId}|File={job.Name}: {ex.GetInnerMostException().Message}");
                        }

                        if (token.IsCancellationRequested)
                            break;
                    }
                    client.Disconnect();
                    logger.Info($"FTP Client {clientId} is Disconnected");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"OnFTPClientStart error for ClientID {clientId}: {ex.GetInnerMostException().Message}");
            }
        }
    }
}
