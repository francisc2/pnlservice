﻿using ServiceStack;
using PnLService.ServiceModel;
using System;
using System.Threading;
using C2Enums;
using System.Linq;

namespace PnLService.ServiceInterface
{
    public class APIServices : Service
    {
        #region Caching

        //https://docs.servicestack.net/http-caching
        //Example
        //[CacheResponse(Duration = 60, MaxAge = 30)]
        //Where subsequent identical requests from a cache-aware client will return their locally cached version within the first 30 seconds
        //, between 30-60 seconds the client will re-validate the request with the Server who will return a 304 NotModified Response with an Empty Body
        //, after 60 seconds the cache expires and the next request will re-execute the Service and populate the cache with a new response.

        #endregion

        private static readonly NLog.Logger logger = NLog.LogManager.GetLogger("APIServices");

        public BaseResponse Any(DownloadFiles request)
        {
            using (var cancellableRequest = base.Request.CreateCancellableRequest())
            {
                BaseResponse response = new BaseResponse();
                
                try
                {
                    cancellableRequest.Token.ThrowIfCancellationRequested();
                    Log.LogRequest($"IN ({this.Request?.OperationName}:{base.Request?.UserHostAddress})", base.Request?.ToSafeJson());
                    using (Database.BrokerAccountInfo.Entities.Context context = new Database.BrokerAccountInfo.Entities.Context())
                    {
                        foreach (var brokerCode in context.BrokerCodes.Where(i => i.Enabled).ToList())
                        {
                            logger.Info($"Downloading files for {brokerCode.Name}");
                            MultiThreadFTPQueue queue = new MultiThreadFTPQueue(cancellableRequest.Token, brokerCode);
                            queue.Run();
                        }
                    }
                    cancellableRequest.TokenSource.Cancel();
                }
                catch (Exception ex)
                {
                    logger.Error(ex, $"{this.Request?.OperationName} error: " + ex.Message);
                    cancellableRequest.TokenSource.Cancel();
                    throw ex;
                }

                Log.LogRequest($"OUT ({this.Request?.OperationName}:{base.Request?.UserHostAddress})", response?.ToSafeJson());
                return response;
            }
        }

        public BaseResponse Any(PnLService.ServiceModel.ProcessFiles request)
        {
            using (var cancellableRequest = base.Request.CreateCancellableRequest())
            {
                BaseResponse response = new BaseResponse();
                
                try
                {
                    Log.LogRequest($"IN ({this.Request?.OperationName}:{base.Request?.UserHostAddress})", base.Request?.ToSafeJson());
                    cancellableRequest.Token.ThrowIfCancellationRequested();

                    using (Database.BrokerAccountInfo.Entities.Context context = new Database.BrokerAccountInfo.Entities.Context())
                    {
                        foreach (var brokerCode in context.BrokerCodes.Where(i => i.Enabled).ToList())
                        {
                            logger.Info($"Launching File Processor");                           
                            ProcessFiles fileProcessor = new ProcessFiles(cancellableRequest.Token, brokerCode);
                            fileProcessor.Run();
                        }
                    }
                    cancellableRequest.TokenSource.Cancel();
                }
                catch (Exception ex)
                {
                    logger.Error(ex, $"{this.Request?.OperationName} error: " + ex.Message);
                    cancellableRequest.TokenSource.Cancel();
                    throw ex;
                }

                Log.LogRequest($"OUT ({this.Request?.OperationName}:{base.Request?.UserHostAddress})", response?.ToSafeJson());
                return response;
            }
        }

        public BaseResponse Any(PnLService.ServiceModel.MoveActivityFiles request)
        {
            using (var cancellableRequest = base.Request.CreateCancellableRequest())
            {
                BaseResponse response = new BaseResponse();
                
                try
                {
                    Log.LogRequest($"IN ({this.Request?.OperationName}:{base.Request?.UserHostAddress})", base.Request?.ToSafeJson());
                    cancellableRequest.Token.ThrowIfCancellationRequested();

                    using (Database.BrokerAccountInfo.Entities.Context context = new Database.BrokerAccountInfo.Entities.Context())
                    {
                        foreach (var brokerCode in context.BrokerCodes.Where(i => i.Enabled).ToList())
                        {
                            logger.Info($"Processing files for {brokerCode.Name}");                           
                            ProcessFiles fileProcessor = new ProcessFiles(cancellableRequest.Token, brokerCode);
                            fileProcessor.MoveActivityFiles();
                        }
                    }
                    cancellableRequest.TokenSource.Cancel();
                }
                catch (Exception ex)
                {
                    logger.Error(ex, $"{this.Request?.OperationName} error: " + ex.Message);
                    cancellableRequest.TokenSource.Cancel();
                    throw ex;
                }

                Log.LogRequest($"OUT ({this.Request?.OperationName}:{base.Request?.UserHostAddress})", response?.ToSafeJson());
                return response;
            }
        }


        [CacheResponse(Duration = 300, MaxAge = 290, VaryByUser = false)]
        public object Any(GetAccountDetails request)
        {
            using (var cancellableRequest = base.Request.CreateCancellableRequest())
            {             
                try
                {
                    cancellableRequest.Token.ThrowIfCancellationRequested();
                    Log.LogRequest($"IN ({this.Request?.OperationName}:{base.Request?.UserHostAddress})", base.Request?.ToSafeJson());
                    var response = request.Get(cancellableRequest.Token, request.BrokerCode);
                    cancellableRequest.TokenSource.Cancel();
                    Log.LogRequest($"OUT ({request.GetType().Name}|{base.Request?.UserHostAddress})", response?.ToSafeJson());
                    return response;
                }
                catch (Exception ex)
                {
                    BaseResponse response = new BaseResponse();
                    string msg = ex.GetInnerMostException().Message;
                    logger.Error(ex, $"{request.GetType().Name} error: {msg}");
                    response.ResponseStatus = new ResponseStatus(System.Net.HttpStatusCode.InternalServerError.ToString(), msg);
                    Log.LogRequest($"OUT ({request.GetType().Name}|{base.Request?.UserHostAddress})", response?.ToSafeJson());
                    return response;
                }               
            }
        }

        public object Any(CMEAudit request)
        {
            using (var cancellableRequest = base.Request.CreateCancellableRequest())
            {             
                try
                {
                    cancellableRequest.Token.ThrowIfCancellationRequested();
                    Log.LogRequest($"IN ({this.Request?.OperationName}:{base.Request?.UserHostAddress})", base.Request?.ToSafeJson());

                    CMEAuditResponse response = (CMEAuditResponse)request.Get(cancellableRequest.Token, request.Account, request.OrderId, request.SignalId);
                    cancellableRequest.TokenSource.Cancel();

                    //Set the file name
                    string dateString = "";
                    string account = request.Account;
                    string symbol = request.Symbol;

                    if (false == string.IsNullOrWhiteSpace(request.FCMAccount))
                        account = $"{request.Account} ({request.FCMAccount})";

                    string startDate = response.Log.Select(i => i.Timestamp).FirstOrDefault();
                    if (startDate == null)
                    {
                        dateString = "No Data";
                    }
                    else
                    {
                        startDate = startDate.Substring(0, 10);
                        string endDate = response.Log.Select(i => i.Timestamp).LastOrDefault();
                        if (endDate == null)
                        {
                            endDate = startDate;
                        }
                        else
                        {
                            endDate = endDate.Substring(0, 10);
                            if (startDate == endDate) dateString = startDate;
                            else dateString = $"{startDate}_{endDate}";
                        }
                    }

                    if (false == string.IsNullOrWhiteSpace(request.Account) && request.SignalId > 0)
                        Request.SetItem("csv-filename", $"{dateString}_Account_{account}_{symbol}_SignalID_{request.SignalId}");
                    else if (false == string.IsNullOrWhiteSpace(request.OrderId) && request.SignalId > 0)
                        Request.SetItem("csv-filename", $"{dateString}_OrderId_{request.OrderId}_{symbol}_SignalID_{request.SignalId}");
                    else if (false == string.IsNullOrWhiteSpace(request.OrderId) && request.SignalId < 1 && false == string.IsNullOrWhiteSpace(request.Account))
                        Request.SetItem("csv-filename", $"{dateString}_Account_{account}_{symbol}_OrderId_{request.OrderId}");
                    else if (string.IsNullOrWhiteSpace(request.OrderId) && string.IsNullOrWhiteSpace(request.Account))
                        Request.SetItem("csv-filename", $"{dateString}_All Accounts for _{symbol}_SignalID_{request.SignalId}");
                    else
                        Request.SetItem("csv-filename", $"Invalid Request");

                    Log.LogRequest($"OUT ({request.GetType().Name}|{base.Request?.UserHostAddress})", response?.ToSafeJson());
                    return response;
                }
                catch (Exception ex)
                {
                    BaseResponse response = new BaseResponse();
                    string msg = ex.GetInnerMostException().Message;
                    logger.Error(ex, $"{request.GetType().Name} error: {msg}");
                    Request.SetItem("csv-filename", $"Error: {ex.Message}");
                    response.ResponseStatus = new ResponseStatus(System.Net.HttpStatusCode.InternalServerError.ToString(), msg);
                    Log.LogRequest($"OUT ({request.GetType().Name}|{base.Request?.UserHostAddress})", response?.ToSafeJson());
                    return response;
                }               
            }
        }
    }
}