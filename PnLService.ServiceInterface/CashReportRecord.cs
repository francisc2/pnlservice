﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PnLService.ServiceInterface
{
    /// <summary>
    /// Version 1.99
    /// </summary>
    public class CashReportRecord
    {
        //Type,AccountID,ReportDate,Currency,BaseSummary,Label,Total,Securities,Futures,IBUKL,
        //D,U7502771,2021-04-19,USD,N,Ending Settled Cash,149044.92069074,149044.92069074,0,0,

        public string Type { get; set; }
        public string AccountID { get; set; }
        public DateTime ReportDate { get; set; }
        public string Currency { get; set; }
        public string BaseSummary { get; set; }
        public string Label { get; set; }
        public decimal Total { get; set; }
        public decimal Securities { get; set; }
        public decimal Futures { get; set; }
        public decimal IBUKL { get; set; }
    }
}
