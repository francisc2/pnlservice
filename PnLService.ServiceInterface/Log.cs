﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PnLService.ServiceInterface
{
    public static class Log
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public static void LogRequest(string msg, string requestStr)
        {
            logger.Trace($"{msg}: {requestStr.Substring(0, Math.Min(requestStr.Length, Properties.Settings.Default.MaxCharToLog))}");
        }
    }
}
