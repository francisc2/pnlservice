﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PnLService.ServiceInterface
{
    internal class IBAssetType
    {
        /// <summary>
        /// Treasury Bills
        /// </summary>
        public const string BILL = "BILL";
        /// <summary>
        /// Bonds (includes corporate and municipal)
        /// </summary>
        public const string BOND = "BOND";
        /// <summary>
        /// Forex
        /// </summary>
        public const string CASH = "CASH";
        /// <summary>
        /// Contracts for Differences
        /// </summary>
        public const string CFD = "CFD";
        /// <summary>
        /// Gold or silver (Metals)
        /// </summary>
        public const string CMDTY = "CMDTY";
        /// <summary>
        /// Dividend Accrual Balance
        /// </summary>
        public const string DIVACC = "DIVACC";
        /// <summary>
        /// Cryptocurrency
        /// </summary>
        public const string CRYPTO = "CRYPTO";
        /// <summary>
        /// Future Options
        /// </summary>
        public const string FOP = "FOP";
        /// <summary>
        /// Options on Futures (Futures Style)
        /// </summary>
        public const string FSFOP = "FSFOP";
        /// <summary>
        /// Options (Futures Style)
        /// </summary>
        public const string FSOPT = "FSOPT";
        /// <summary>
        /// Mutual Funds
        /// </summary>
        public const string FUND = "FUND";
        /// <summary>
        /// Futures
        /// </summary>
        public const string FUT = "FUT";
        /// <summary>
        /// Contract for Difference on forex
        /// </summary>
        public const string FXCFD = "FXCFD";
        /// <summary>
        /// FX Forwards
        /// </summary>
        public const string FXFWD = "FXFWD";
        /// <summary>
        /// IB Notes
        /// </summary>
        public const string IBGNOTE = "IBGNOTE";
        /// <summary>
        /// Interest Accrual Balance
        /// </summary>
        public const string INTACC = "INTACC";
        /// <summary>
        /// Structured Products
        /// </summary>
        public const string IOPT = "IOPT";
        /// <summary>
        /// Equity and Index Options
        /// </summary>
        public const string OPT = "OPT";
        /// <summary>
        /// Stocks (includes ADR, ETF)
        /// </summary>
        public const string STK = "STK";
        /// <summary>
        /// Warrants
        /// </summary>
        public const string WAR = "WAR";
    }
}
