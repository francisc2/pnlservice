﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PnLService.ServiceInterface
{
    /// <summary>
    /// Version 1.99
    /// </summary>
    public class SecurityRecord
    {
        //Type,ConID,AssetType,SecurityID,Symbol,BBTicker,BBTickerAndExchangeCode,BBGlobalID,Description,UnderlyingSymbol,UnderlyingCategory,UnderlyingSecurityId,UnderlyingPrimaryExchange,UnderlyingConID,Multiplier,ExpirationDate,OptionType,OptionStrike,MaturityDate,IssueDate,PrimaryExchange,Currency,SubCategory,Issuer,
        //D,10098,STK,US0605051046,BAC,BAC,BAC US,BBG000BCTLF6,BANK OF AMERICA CORP,,,,,,1,,,,,,NYSE,USD,COMMON,,

        /// <summary>
        /// D
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// Account number
        /// </summary>
        [OptionalField]
        public string AccountID = null;
        /// <summary>
        /// Internal unique identifier for all asset types. 
        /// Full asset information can be found in the Security File Layout.
        /// Empty for Asset Type CASH.
        /// </summary>
        public string ConID { get; set; }
        /// <summary>
        /// For more information, see Asset Types.
        /// </summary>
        public string AssetType { get; set; }
        /// <summary>
        /// ISIN for US and nonUS securities
        /// </summary>
        public string SecurityID { get; set; }
        /// <summary>
        /// Empty for Asset Type CASH
        /// </summary>
        public string Symbol { get; set; }
        /// <summary>
        /// Bloomberg ticker
        /// </summary>
        [OptionalField]
        public string BBTicker = null;
        /// <summary>
        /// Bloomberg ticker + exchange code
        /// </summary>
        [OptionalField]
        public string BBTickerAndExchangeCode = null;
        /// <summary>
        /// Bloomberg Global ID
        /// </summary>
        [OptionalField]
        public string BBGlobalID = null;

        [OptionalField]
        public string CUSIP = null;
        /// <summary>
        /// Description of the security
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// For derivatives
        /// </summary>
        [OptionalField]
        public string UnderlyingSymbol = null;
        /// <summary>
        /// For derivatives
        /// </summary>
        [OptionalField]
        public string UnderlyingCategory = null;
        /// <summary>
        /// For derivatives
        /// </summary>
        [OptionalField]
        public string UnderlyingSecurityID = null;
        /// <summary>
        /// For derivatives
        /// </summary>
        [OptionalField]
        public string UnderlyingPrimaryExchange = null;
        /// <summary>
        /// For derivatives
        /// </summary>
        [OptionalField]
        public string UnderlyingConID = null;
        /// <summary>
        /// 
        /// </summary>
        public string Multiplier { get; set; }
        /// <summary>
        /// CCYY-MM-DD Option or Future expiration date (date format configurable)
        /// </summary>
        public string ExpirationDate { get; set; }
        /// <summary>
        /// P for Put, C for Call
        /// </summary>
        public string OptionType { get; set; }
        /// <summary>
        /// Strike price
        /// </summary>
        public string OptionStrike { get; set; }
        /// <summary>
        /// Bond maturity date
        /// </summary>
        public string MaturityDate { get; set; }
        /// <summary>
        /// Note issue date
        /// </summary>
        public string IssueDate { get; set; }
        /// <summary>
        /// The exchange the security is listed on (stocks only).
        /// </summary>
        [OptionalField]
        public string PrimaryExchange = null;
        /// <summary>
        /// Currency of the asset
        /// </summary>
        [OptionalField]
        public string Currency = null;
        /// <summary>
        /// Available for stocks
        /// </summary>
        [OptionalField]
        public string SubCategory = null;
        /// <summary>
        /// Security issuer
        /// </summary>
        [OptionalField]
        public string Issuer = null;
        [OptionalField]
        public string DeliveryMonth = null;
    }
}
