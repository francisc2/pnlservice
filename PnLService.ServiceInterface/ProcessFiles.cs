﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Threading.Tasks;
using PgpCore;
using System.IO;
using System.Net;
using ServiceStack;
using System.Threading;
using System.Diagnostics;
using CsvHelper;
using CsvHelper.Configuration;
using Database.BrokerAccountInfo.Entities;
using System.Text.RegularExpressions;
using InstrumentUtils;
using C2Common;
using System.Data.Entity;

namespace PnLService.ServiceInterface
{

    /// <summary>
    /// b) parse them and store values in the database
    /// c) mark processed files so they are not processed again.
    /// d) keep all files locally and purge them once in a while as the SSD gets full.
    /// e) files are downloaded in RAW folder, then moved and decrypted into the Decrypted folder.
    /// f) ignored files (those we don't process) are moved as-is from RAW to the UnprocessedArchive folder.
    /// This endpoint will be called after DownloadFiles.
    /// We can also call it at any time manually though a web request.
    /// See https://www.ibkrguides.com/reportingintegration/topics/
    /// </summary>
    public class ProcessFiles
    {
        static readonly NLog.Logger logger = NLog.LogManager.GetLogger("ProcessFiles");
        CancellationToken token = CancellationToken.None;
        DirectoryInfo rawDirectoryInfo, activityRawDirectoryInfo, archiveDirectoryInfo, keyDirectoryInfo, decryptedDirectoryInfo, invalidFilesDirectoryInfo;
        BrokerCode brokerCode;
        Dictionary<SymbolMappingKey, Database.Entities.SymbolMapping> brokerToC2Mappings;


        public ProcessFiles(CancellationToken token, BrokerCode brokerCode)
        {
            this.token = token;
            this.brokerCode = brokerCode;
            this.brokerToC2Mappings = LoadSymbolMappings();

            //Make sure local paths exist
            keyDirectoryInfo = Directory.CreateDirectory($"{Properties.Settings.Default.LocalFolder}\\Keys");
            rawDirectoryInfo = Directory.CreateDirectory($"{Properties.Settings.Default.LocalFolder}\\{brokerCode.Name}\\Raw");
            archiveDirectoryInfo = Directory.CreateDirectory($"{Properties.Settings.Default.LocalFolder}\\{brokerCode.Name}\\UnprocessedArchive");
            decryptedDirectoryInfo = Directory.CreateDirectory($"{Properties.Settings.Default.LocalFolder}\\{brokerCode.Name}\\Decrypted");
            activityRawDirectoryInfo = Directory.CreateDirectory($"{Properties.Settings.Default.LocalFolder}\\{brokerCode.Name}\\ActivityRaw");
            invalidFilesDirectoryInfo = Directory.CreateDirectory($"{Properties.Settings.Default.LocalFolder}\\{brokerCode.Name}\\InvalidFiles");
        }

        public void Run()
        {
            long getallFiles = 0;
            long decryptFiles = 0;
            long castRowsToObjects = 0;
            long processFiles = 0;
            long saveToDb = 0;
            long moveFiles = 0;
            long deleteFiles = 0;
            Stopwatch sw = Stopwatch.StartNew();

            #region Get a list of files

            //Process security files first to store instrument data
            var allRawFiles = rawDirectoryInfo.EnumerateFiles("*.csv.pgp").OrderByDescending(i => i.Name.IndexOf("_SECURITY_", StringComparison.OrdinalIgnoreCase) > -1).ToList();
            List<string> filesToDelete = new List<string>();

            int numFiles = allRawFiles.Count();
            if (numFiles == 0)
            {
                logger.Info($"No {brokerCode.Name} files to process");
                return;
            }
            else
            {
                logger.Info($"Processing {numFiles} {brokerCode.Name} files");
            }

            getallFiles = sw.ElapsedMilliseconds;
            sw.Reset();

            #endregion

            using (FileStream privateKeyStream = new FileStream($@"{keyDirectoryInfo.FullName}\\{brokerCode.KeyFilename}", FileMode.Open))
            {
                var decryptionKey = new EncryptionKeys(privateKeyStream.ReadToEnd(), brokerCode.KeyPassphrase);
                var readerConf = new CsvHelper.Configuration.CsvConfiguration(CultureInfo.InvariantCulture)
                {
                    Delimiter = ",",
                    HasHeaderRecord = true,
                };

                List<SecurityRecord> securityRecords = new List<SecurityRecord>();

                //Loop through files.
                //If any record in a file cannot be processed due to error
                //, the file is kept in the RAW folder and the pending db changes are discarded.
                foreach (var file in allRawFiles)
                {
                    using (var context = new Database.BrokerAccountInfo.Entities.Context())
                    {
                        //Store the row for logging in case of errors
                        object currentRow = null;

                        try
                        {
                            if (token.IsCancellationRequested)
                                break;

                            logger.Info($"Processing RAW file: {file.Name}");

                            #region Skip if not Activity, Account, CashReport and NAV

                            FileType fileType = Utils.GetFileType(file.Name);
                            if (fileType != FileType.Activity
                                && fileType != FileType.Account
                                && fileType != FileType.NAV
                                && fileType != FileType.CashReport
                                && fileType != FileType.Security)
                            {
                                logger.Warn($"{file.Name} is not supported but it is in the RAW folder. Moving it to the UnprocessedArchive folder for safekeeping");
                                file.MoveTo($"{archiveDirectoryInfo.FullName}\\{file.Name}");
                                continue;
                            }

                            #endregion

                            #region Decrypt the file

                            //See https://github.com/mattosaurus/PgpCore
                            sw.Start();
                            string decryptedString;
                            using (PGP pgp = new PGP(decryptionKey))
                            {
                                using (FileStream inputFileStream = file.OpenRead())
                                using (MemoryStream outputStream = new MemoryStream())
                                {
                                    if (file.Length == 0)
                                    {
                                        File.Delete(file.FullName);
                                        continue;
                                    }
                                    else
                                    {
                                        pgp.DecryptStream(inputFileStream, outputStream);
                                        decryptedString = outputStream.ReadToEnd();
                                    }
                                }
                            }

                            //if (decryptedString.Length > 0)
                            //{
                            //    logger.Debug($"Decrypted file: {decryptedString}");
                            //}

                            decryptFiles += sw.ElapsedMilliseconds;
                            sw.Reset();

                            #endregion

                            #region Process File

                            if (decryptedString.Length == 0)
                            {
                                File.Delete(file.FullName);
                                continue;
                            }

                            #region Process Header (deprecated--they are not sending it)

                            //if (false == csv.Read())
                            //{
                            //    logger.Debug($"Skipping empty file: {file.Name}.");
                            //    continue;
                            //}

                            //if (false == csv.ReadHeader())
                            //{
                            //    logger.Debug($"Missing header. Skipping file: {file.Name}.");
                            //    continue;
                            //}

                            //if (csv.HeaderRecord == null || csv.HeaderRecord.Length < 7)
                            //{
                            //    logger.Info($"Can't read the file header: {string.Join("|", csv.HeaderRecord)}. Skipping {file.Name}.");
                            //    continue;
                            //}

                            //if (false == decimal.TryParse((string)csv.HeaderRecord.GetValue(5), out decimal version) 
                            //    || version <= 0)
                            //{
                            //    logger.Error($"Error reading the file version: HeaderRecord={string.Join("|", csv.HeaderRecord)}");
                            //    continue;
                            //}
                            //logger.Debug($"Version {version}");

                            #endregion

                            //logger.Debug($"Processing {file.Name}");
                            if (fileType == FileType.Account)
                            {
                                //Type,AccountID,AccountTitle,Street,Street2,City,State,Zip,Country,AccountType,CustomerType,BaseCurrency,MasterAccountID,Van,Capabilities,Alias,PrimaryEmail,DateOpened,DateClosed,DateFunded,AccountRepresentative,
                                //D,U5426118,Klara Zentai,Fodor Utca 50/A,1 em 5 a,Budapest 12,HU-BU,1124,Hungary,Individual,Individual,EUR,,,Margin,,zentaiklara1@gmail.com,2021-02-11,,2021-02-10,,
                                using (var reader = new StreamReader(new MemoryStream(decryptedString.ToAsciiBytes())))
                                using (var csv = new CsvReader(reader, readerConf, false))
                                {
                                    #region Process Rows

                                    sw.Start();
                                    var rows = csv.GetRecords<AccountRecord>();
                                    castRowsToObjects += sw.ElapsedMilliseconds;
                                    sw.Reset();

                                    foreach (var row in rows)
                                    {
                                        sw.Start();
                                        bool isNewRow = false;

                                        //Only add if new
                                        var entity = context.Accounts.Where(i => i.AccountId == row.AccountID).FirstOrDefault();
                                        if (entity == null)
                                        {
                                            isNewRow = true;
                                            entity = context.Accounts.Create();
                                        }

                                        entity.AccountId = row.AccountID;
                                        entity.AccountTitle = row.AccountTitle;
                                        entity.AccountType = row.AccountType;
                                        entity.BaseCurrency = row.BaseCurrency;
                                        entity.BrokerCode = GetBrokerCodeFromFileName(file.Name, brokerCode.Code);
                                        entity.Capabilities = row.Capabilities;
                                        entity.City = row.City;
                                        entity.Country = row.Country;
                                        entity.CustomerType = row.CustomerType;
                                        entity.Email = row.PrimaryEmail;
                                        entity.MasterAccountId = row.MasterAccountID;
                                        entity.State = row.State;
                                        entity.Street = row.Street;
                                        entity.Street2 = row.Street2;
                                        entity.Zip = row.Zip;

                                        if (DateTime.TryParseExact(row.DateOpened, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime DateOpened))
                                            if (entity.DateOpened != DateOpened) entity.DateOpened = DateOpened;

                                        if (DateTime.TryParseExact(row.DateClosed, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime DateClosed))
                                            if (entity.DateClosed != DateClosed) entity.DateClosed = DateClosed;

                                        if (DateTime.TryParseExact(row.DateFunded, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime DateFunded))
                                            if (entity.DateFunded != DateFunded) entity.DateFunded = DateFunded;


                                        processFiles += sw.ElapsedMilliseconds;
                                        sw.Reset();

                                        if (isNewRow)
                                        {
                                            sw.Start();
                                            context.Accounts.Add(entity);
                                            saveToDb += sw.ElapsedMilliseconds;
                                            sw.Reset();
                                        }
                                    }

                                    #endregion
                                }

                                #region Old Manual code
                                //using (var reader = new StreamReader(new MemoryStream(decryptedString.ToAsciiBytes())))
                                //                     using (var csv = new CsvReader(reader, config))
                                //                     {
                                //                         while (csv.Read())
                                //                         {
                                //                             var item = context.Accounts.Create();
                                //                             item.AccountId = csv.GetField(1);

                                //                             //Only add if new
                                //                             var existing = context.Accounts.Where(i => i.AccountId == item.AccountId).FirstOrDefault();
                                //                             if (existing != null)
                                //                                 item = existing;
                                //                             else
                                //                                 context.Accounts.Add(item);

                                //                             if (item.AccountType != csv.GetField(0)) item.AccountType = csv.GetField(0);
                                //                             if (item.AccountTitle != csv.GetField(2)) item.AccountTitle = csv.GetField(2);
                                //                             if (item.Street != csv.GetField(3)) item.Street = csv.GetField(3);
                                //                             if (item.Street2 != csv.GetField(4)) item.Street2 = csv.GetField(4);
                                //                             if (item.City != csv.GetField(5)) item.City = csv.GetField(5);
                                //                             if (item.State != csv.GetField(6)) item.State = csv.GetField(6);
                                //                             if (item.Zip != csv.GetField(7)) item.Zip = csv.GetField(7);
                                //                             if (item.Country != csv.GetField(8)) item.Country = csv.GetField(8);
                                //                             if (item.CustomerType != csv.GetField(10)) item.CustomerType = csv.GetField(10);
                                //                             if (item.BaseCurrency != csv.GetField(11)) item.BaseCurrency = csv.GetField(11);
                                //                             if (item.MasterAccountId != csv.GetField(12)) item.MasterAccountId = csv.GetField(12);
                                //                             if (item.Capabilities != csv.GetField(14)) item.Capabilities = csv.GetField(14);
                                //                             if (item.Email != csv.GetField(16)) item.Email = csv.GetField(16);
                                //                             if (DateTime.TryParseExact(csv.GetField(17), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime DateOpened))
                                //                                 if (item.DateOpened != DateOpened) item.DateOpened = DateOpened;

                                //                             if (DateTime.TryParseExact(csv.GetField(18), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime DateClosed))
                                //                                 if (item.DateClosed != DateClosed) item.DateClosed = DateClosed;

                                //                             if (DateTime.TryParseExact(csv.GetField(19), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime DateFunded))
                                //                                 if (item.DateFunded != DateFunded) item.DateFunded = DateFunded;

                                //                             item.BrokerCode = GetBrokerCodeFromFileName(file.Name, brokerCode.Code);
                                //                             context.SaveChanges();
                                //                         }
                                //}
                                #endregion
                            }
                            else if (fileType == FileType.NAV)
                            {
                                //Type	AccountID	BaseCurrency	Cash	CashCollateral	Stocks	SecuritiesBorrowed	SecuritiesLent	Options	Bonds	Commodities	Funds	Notes	Accruals	DividendAccruals	SoftDollars	Totals	TWR	CFDUnrealizedPL	ForexCFDUnrealizedPL	IPOSubscription
                                //D	U000001	USD	3290021.047	1194746	131578.29	0	-1194746	-206046.7	0	0	0	0	-3665.444604	1200	0	3213087.192	0.714319828	0	0	0
                                //D,U481497,USD,99934.79163819,0,0,0,0,0,0,0,0,0,-602.65569626,0,0,99332.13594193,-2.703418192,0,0,0,

                                using (var reader = new StreamReader(new MemoryStream(decryptedString.ToAsciiBytes())))
                                using (var csv = new CsvReader(reader, readerConf, false))
                                {
                                    #region Process Rows

                                    sw.Start();
                                    var rows = csv.GetRecords<NAVRecord>();
                                    castRowsToObjects += sw.ElapsedMilliseconds;
                                    sw.Reset();

                                    foreach (var row in rows)
                                    {
                                        sw.Start();
                                        var entity = context.Navs.Create();

                                        entity.AccountId = row.AccountID;
                                        entity.BaseCurrency = row.BaseCurrency;
                                        entity.Cash = row.Cash;
                                        entity.CashCollateral = row.CashCollateral;
                                        entity.Stocks = row.Stocks;
                                        entity.SecuritiesBorrowed = row.SecuritiesBorrowed;
                                        entity.SecuritiesLent = row.SecuritiesLent;
                                        entity.Options = row.Options;
                                        entity.Bonds = row.Bonds;
                                        entity.Commodities = row.Commodities;
                                        entity.Funds = row.Funds;
                                        entity.Notes = row.Notes;
                                        entity.Accruals = row.Accruals;
                                        entity.DividendAccruals = row.DividendAccruals;
                                        entity.SoftDollars = row.SoftDollars;
                                        entity.Totals = row.Totals;
                                        entity.Twr = row.TWR;
                                        entity.CfdUnrealizedPL = row.CFDUnrealizedPL;
                                        //entity.ForexUnrealizedPL = row.ForexUnrealizedPL;
                                        //entity.IpoSubscription = row.IPOSubscription;
                                        //entity.InsuredBankDepositSweepProgram = row.InsuredBankDepositSweepProgram;
                                        //entity.InsuredBankDepositSweepProgramIntAccrurals = row.InsuredBankDepositSweepProgramIntAccruals;
                                        entity.Date = GetDateFromFileName(Path.GetFileNameWithoutExtension(file.Name));

                                        processFiles += sw.ElapsedMilliseconds;
                                        sw.Reset();

                                        sw.Start();
                                        context.Navs.Add(entity);
                                        saveToDb += sw.ElapsedMilliseconds;
                                        sw.Reset();
                                    }

                                    #region Old manual method
                                    //while (csv.Read())
                                    //{
                                    //    //Headers
                                    //    if (++rowNumber == 1)
                                    //    {
                                    //        continue;
                                    //    }

                                    //    var item = context.Navs.Create();
                                    //    item.AccountId = csv.GetField(1);
                                    //    item.BaseCurrency = csv.GetField(2);
                                    //    item.Cash = ParseDecimal(csv.GetField(3));
                                    //    item.CashCollateral = ParseDecimal(csv.GetField(4));
                                    //    item.Stocks = ParseDecimal(csv.GetField(5));
                                    //    item.SecuritiesBorrowed = ParseDecimal(csv.GetField(6));
                                    //    item.SecuritiesLent = ParseDecimal(csv.GetField(7));
                                    //    item.Options = ParseDecimal(csv.GetField(8));
                                    //    item.Bonds = ParseDecimal(csv.GetField(9));
                                    //    item.Commodities = ParseDecimal(csv.GetField(10));
                                    //    item.Funds = ParseDecimal(csv.GetField(11));
                                    //    item.Notes = ParseDecimal(csv.GetField(12));
                                    //    item.Accruals = ParseDecimal(csv.GetField(13));
                                    //    item.DividendAccruals = ParseDecimal(csv.GetField(14));
                                    //    item.SoftDollars = ParseDecimal(csv.GetField(15));
                                    //    item.Totals = ParseDecimal(csv.GetField(18));
                                    //    item.Twr = ParseDecimal(csv.GetField(19));
                                    //    item.ForexUnrealizedPL = ParseDecimal(csv.GetField(20));
                                    //    item.Date = GetDateFromFileName(Path.GetFileNameWithoutExtension(file.Name));

                                    //    context.Navs.Add(item);
                                    //    context.SaveChanges();
                                    //}

                                    #endregion

                                    #endregion
                                }
                            }
                            else if (fileType == FileType.CashReport)
                            {
                                /*
                                 * Type,AccountID,ReportDate,Currency,BaseSummary,Label,Total,Securities,Futures,IBUKL,
                                    D,U7502771,2021-04-19,USD,N,Ending Settled Cash,149044.92069074,149044.92069074,0,0,
                                    D,U7502771,2021-04-19,USD,Y,Commissions,-18.77,-18.77,0,0,
                                */

                                using (var reader = new StreamReader(new MemoryStream(decryptedString.ToAsciiBytes())))
                                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                                {
                                    #region Process Rows

                                    sw.Start();
                                    var rows = csv.GetRecords<CashReportRecord>();
                                    castRowsToObjects += sw.ElapsedMilliseconds;
                                    sw.Reset();

                                    foreach (var row in rows)
                                    {
                                        sw.Start();

                                        //Only process base currency
                                        if (row.BaseSummary != "Y")
                                            continue;

                                        //Only add if new
                                        bool isNewRow = false;
                                        var entity = context.Cashreports.Where(i => i.Date == row.ReportDate && i.AccountId == row.AccountID && i.BaseCurrency == row.Currency).FirstOrDefault();
                                        if (entity == null)
                                        {
                                            isNewRow = true;
                                            entity = context.Cashreports.Create();
                                        }

                                        entity.Date = row.ReportDate;
                                        entity.AccountId = row.AccountID;
                                        entity.BaseCurrency = row.Currency;
                                        //entity.Futures = row.Futures;
                                        //entity.IBUKL = row.IBUKL;
                                        //entity.Securities = row.Securities;
                                        //entity.Total = row.Total;

                                        if (row.Label.EqualsIgnoreCase("Starting Cash"))
                                            entity.StartingCash = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Commissions"))
                                            entity.Commissions = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Internal Transfers"))
                                            entity.InternalTransfers = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Account Transfers"))
                                            entity.AccountTransfers = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Cash Settling MTM"))
                                            entity.CashSettlingMTM = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Net Trades (Sales)"))
                                            entity.NetTradesSales = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Net Trades (Purchase)"))
                                            entity.NetTradesPurchases = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Cash FX Translation Gain/Loss"))
                                            entity.CashFxGainLoss = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Ending Cash"))
                                            entity.EndingCash = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Ending Settled Cash"))
                                            entity.EndingSettledCash = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Deposits"))
                                            entity.Deposits = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Withdrawals"))
                                            entity.Withdrawals = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Debit Card Activity"))
                                            entity.DebitCardActivity = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Broker Interest Paid and Received"))
                                            entity.BrokerInterest = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Transaction Fees"))
                                            entity.TransactionFees = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Other Fees"))
                                            entity.OtherFees = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Change in Insured Bank Deposits"))
                                            entity.ChangeInInsuredBankDeposits = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Dividends"))
                                            entity.Dividends = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Payment In Lieu of Dividends"))
                                            entity.PaymentInLieuOfDividends = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Advisor Fees"))
                                            entity.AdvisorFees = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Client Fees"))
                                            entity.ClientFees = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("GST/HST/PST")
                                            || row.Label.EqualsIgnoreCase("Withholding Tax")
                                            || row.Label.EqualsIgnoreCase("Insured Deposit Interest")
                                            || row.Label.EqualsIgnoreCase("Commission Credits Redemption")
                                            || row.Label.EqualsIgnoreCase("CFD Charges")
                                            || row.Label.EqualsIgnoreCase("CFD Realized P/L")
                                            || row.Label.EqualsIgnoreCase("Broker Fees Paid and Received")
                                            || row.Label.EqualsIgnoreCase("Bond Interest Paid and Received")
                                            || row.Label.EqualsIgnoreCase("Sales Tax")
                                            || row.Label.EqualsIgnoreCase("Forex CFD Realized P/L")
                                            || row.Label.EqualsIgnoreCase("Debit/Prepaid Card Activity")
                                            || row.Label.EqualsIgnoreCase("Bill Pay")
                                            || row.Label.EqualsIgnoreCase("IPO Subscription")
                                            || row.Label.EqualsIgnoreCase("CYEP/Broker Fees Paid and Received")
                                            || row.Label.EqualsIgnoreCase("871(m) Withholding"))
                                            continue;
                                        else
                                            throw new Exception($"Unknown field '{row.Label}' in {file.Name} | File={decryptedString}");

                                        processFiles += sw.ElapsedMilliseconds;
                                        sw.Reset();

                                        if (isNewRow)
                                        {
                                            sw.Start();
                                            context.Cashreports.Add(entity);
                                            saveToDb += sw.ElapsedMilliseconds;
                                            sw.Reset();
                                        }
                                    }

                                    #endregion
                                }

                                #region Old Manual code
                                //using (var reader = new StreamReader(new MemoryStream(decryptedString.ToAsciiBytes())))
                                //using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                                //{
                                //    var item = context.Cashreports.Create();

                                //    while (csv.Read())
                                //    {
                                //        //Only process base currency
                                //        if (csv.GetField(4) != "Y")
                                //            continue;

                                //        item.AccountId = csv.GetField(1);

                                //        DateTime.TryParseExact(csv.GetField(2), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime ReportDate);
                                //        item.Date = ReportDate;

                                //        item.BaseCurrency = csv.GetField(3);

                                //        if (csv.GetField(5).EqualsIgnoreCase("Starting Cash"))
                                //            item.StartingCash = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Commissions"))
                                //            item.Commissions = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Internal Transfers"))
                                //            item.InternalTransfers = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Account Transfers"))
                                //            item.AccountTransfers = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Cash Settling MTM"))
                                //            item.CashSettlingMTM = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Net Trades (Sales)"))
                                //            item.NetTradesSales = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Net Trades (Purchase)"))
                                //            item.NetTradesPurchases = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Cash FX Translation Gain/Loss"))
                                //            item.CashFxGainLoss = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Ending Cash"))
                                //            item.EndingCash = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Ending Settled Cash"))
                                //            item.EndingSettledCash = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Deposits"))
                                //            item.Deposits = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Withdrawals"))
                                //            item.Withdrawals = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Debit Card Activity"))
                                //            item.DebitCardActivity = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Broker Interest Paid and Received"))
                                //            item.BrokerInterest = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Transaction Fees"))
                                //            item.TransactionFees = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Other Fees"))
                                //            item.OtherFees = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Change in Insured Bank Deposits"))
                                //            item.ChangeInInsuredBankDeposits = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Dividends"))
                                //            item.Dividends = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Payment In Lieu of Dividends"))
                                //            item.PaymentInLieuOfDividends = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Advisor Fees"))
                                //            item.AdvisorFees = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Client Fees"))
                                //            item.ClientFees = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("GST/HST/PST")
                                //            || csv.GetField(5).EqualsIgnoreCase("Withholding Tax")
                                //            || csv.GetField(5).EqualsIgnoreCase("Insured Deposit Interest")
                                //            || csv.GetField(5).EqualsIgnoreCase("Commission Credits Redemption")
                                //            || csv.GetField(5).EqualsIgnoreCase("CFD Charges")
                                //            || csv.GetField(5).EqualsIgnoreCase("CFD Realized P/L")
                                //            || csv.GetField(5).EqualsIgnoreCase("Bond Interest Paid and Received")
                                //            || csv.GetField(5).EqualsIgnoreCase("Sales Tax")
                                //            || csv.GetField(5).EqualsIgnoreCase("Forex CFD Realized P/L")
                                //            || csv.GetField(5).EqualsIgnoreCase("Debit/Prepaid Card Activity"))
                                //            continue;
                                //        else
                                //            throw new Exception($"Unknown field '{csv.GetField(5)}' in {file.Name} | File={decryptedString}");
                                //    }

                                //    context.Cashreports.Add(item);
                                //    context.SaveChanges();
                                //}
                                #endregion

                            }
                            else if (fileType == FileType.Security)
                            {
                                //SecurityID is missing for options so don't use it.
                                //Type	ConID	AssetType	SecurityID	    Symbol	            BBTicker	BBTickerAndExchangeCode	BBGlobalID	Description	UnderlyingSymbol	UnderlyingCategory	UnderlyingSecurityId	UnderlyingPrimaryExchange	UnderlyingConID	Multiplier	ExpirationDate	OptionType	OptionStrike	MaturityDate	IssueDate	PrimaryExchange	Currency	SubCategory	Issuer
                                //D	496666584	OPT		                    TSP 210716C00065000	TSP 7 C65	BBG011F15J91	TSP 16JUL21 65.0 C	TSP	STK	US90089L1089	NASDAQ	482017067	100	2021-07-16	C	65			CBOE	USD		
                                //D	7765	    STK	    US3647601083	    GPS	                GPS	GPS US	BBG000BKLH74	GAP INC/THE						1						NYSE	USD	COMMON	
                                //D	44495102	CASH	USD.ILS			    USD.ILS						                    1							ILS		
                                //D	568549458	FUT		                    MNQU3	            HWBU3		BBG0183YHVW1	MNQ 15SEP23	MNQ	IND			362687422	2	2023-09-15					CME	USD		


                                using (var reader = new StreamReader(new MemoryStream(decryptedString.ToAsciiBytes())))
                                using (var csv = new CsvReader(reader, readerConf, false))
                                {
                                    #region Process Rows

                                    //Sometimes there is no header in the file so we can't parse it reliably (fields may or may not be present, etc)
                                    sw.Start();
                                    IEnumerable<SecurityRecord> rows = csv.GetRecords<SecurityRecord>();
                                    castRowsToObjects += sw.ElapsedMilliseconds;
                                    sw.Reset();

                                    try
                                    {
                                        foreach (var row in rows)
                                        {
                                            if (false == securityRecords.Contains(row))
                                                securityRecords.Add(row);
                                        }
                                    }
                                    catch (HeaderValidationException ex)
                                    {
                                        if (ex.Message.IndexOf("Header with name 'Type'[0] was not found", StringComparison.OrdinalIgnoreCase) > -1)
                                        {
                                            //Ignore this file but save it in a junk file folder
                                            File.WriteAllText($"{invalidFilesDirectoryInfo.FullName}\\{file.Name.Remove(file.Name.Length - 4)}", decryptedString, Encoding.UTF8);
                                            filesToDelete.Add(file.FullName);
                                            continue;
                                        }
                                        else
                                        {
                                            throw ex;
                                        }
                                    }

                                    #endregion
                                }
                            }
                            else if (fileType == FileType.Activity)
                            {
                                //API Version 1.99
                                //Type,AccountID,ConID,SecurityID,Symbol,BBTicker,BBGlobalID,SecurityDescription,AssetType,Currency,BaseCurrency,TradeDate,TradeTime,SettleDate,TransactionType,Quantity,UnitPrice,GrossAmount,SECFee,Commission,Tax,Net,NetInBase,TradeID,TaxBasisElection,Description,FxRateToBase,ContraPartyName,ClrFirmID,Exchange,MasterAccountID,Van,AwayBrokerCommission,OrderID,ClientReference,TransactionID,
                                //D,F1867187,,,,,,,,USD,USD,2021-06-27,18:00:00,2021-06-27,ADJ,0,0,2.99,0,0,0,2.99,2.99,777058450,,"CLIENT FEE (U4694431, Commission)",1,,,,I533594,,0,,,,

                                using (var reader = new StreamReader(new MemoryStream(decryptedString.ToAsciiBytes())))
                                using (var csv = new CsvReader(reader, readerConf, false))
                                {
                                    #region Process Rows

                                    sw.Start();
                                    var rows = csv.GetRecords<ActivityRecord>();
                                    castRowsToObjects += sw.ElapsedMilliseconds;
                                    sw.Reset();

                                    foreach (ActivityRecord row in rows)
                                    {
                                        #region Row

                                        sw.Start();
                                        currentRow = row;
                                        var entity = new Database.BrokerAccountInfo.Entities.Activity();
                                        entity.Type = row.Type;
                                        entity.AccountId = row.AccountID;
                                        entity.AssetType = row.AssetType;
                                        entity.BaseCurrency = row.BaseCurrency;
                                        entity.Currency = row.Currency;
                                        entity.Commission = row.Commission;
                                        entity.Currency = row.Currency;
                                        entity.ConId = row.ConID;
                                        entity.Description = row.Description;
                                        entity.GrossAmount = row.GrossAmount;
                                        entity.Net = row.Net;
                                        entity.NetInBase = row.NetInBase;
                                        entity.Quantity = row.Quantity;
                                        entity.SecFee = row.SECFee;
                                        entity.SecurityId = row.SecurityID;
                                        entity.Symbol = row.Symbol;
                                        entity.Tax = row.Tax;
                                        entity.TIBrokerCode = GetBrokerCodeFromFileName(file.Name, brokerCode.Code);
                                        entity.TradeId = row.TradeID;
                                        entity.TaxBasisElection = row.TaxBasisElection;
                                        entity.TransactionType = row.TransactionType;
                                        entity.UnitPrice = row.UnitPrice;
                                        entity.FxRateToBase = row.FXRatetoBase;
                                        entity.ContraPartyName = row.ContraPartyName;
                                        entity.ClrFirmId = row.ClrFirmID;
                                        entity.Exchange = row.Exchange;
                                        entity.MasterAccountId = row.MasterAccountID;
                                        entity.Van = row.Van;
                                        entity.SecurityDescription = row.SecurityDescription;
                                        entity.AwayBrokerCommission = row.AwayBrokerCommission;
                                        entity.OrderId = row.OrderID;
                                        entity.BBGlobalId = row.BBGlobalID;
                                        entity.BBTicker = row.BBTicker;
                                        entity.ClientReference = row.ClientReference;
                                        entity.TransactionId = row.TransactionID;
                                        entity.CostBasis = row.CostBasis;
                                        entity.ExecutionId = row.ExecutionID;
                                        entity.Flag = row.Flag;

                                        if (row.TradeDate != null
                                            && DateTime.TryParseExact(row.TradeDate, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime tradeDate))
                                        {
                                            if (false == entity.TradeDate.HasValue) entity.TradeDate = tradeDate;
                                        }

                                        if (row.SettleDate != null
                                            && DateTime.TryParseExact(row.SettleDate, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime settleDate))
                                        {
                                            if (false == entity.SettleDate.HasValue) entity.SettleDate = settleDate;
                                        }

                                        if (false == string.IsNullOrWhiteSpace(row.TradeTime)
                                            && DateTime.TryParseExact(row.TradeTime, "HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime tradeTime))
                                        {
                                            entity.TradeDate = entity.TradeDate.Value.Add(tradeTime.TimeOfDay);
                                        }

                                        //TODO: what is this used for? Where is orderdate?
                                        if (false == string.IsNullOrWhiteSpace(row.OrderTime)
                                            && DateTime.TryParseExact(row.OrderTime, "HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime orderTime))
                                        {
                                            if (entity.OrderDate.HasValue) entity.OrderDate = entity.OrderDate.Value.Add(orderTime.TimeOfDay);
                                        }

                                        #endregion

                                        #region Instrument

                                        if (false == string.IsNullOrWhiteSpace(row.AssetType))
                                        {
                                            Database.Entities.SymbolMapping mapping = null;
                                            Instrument instr = null;
                                            var secType = Utils.IBAssetTypeToFIXSecurityType(row.AssetType);

                                            if (secType != null)
                                            {
                                                if (entity.AssetType == IBAssetType.OPT)
                                                {
                                                    if (row.Currency != "USD")
                                                    {
                                                        logger.Debug($"Activity: Non-USD options are not supported by C2: {row.Symbol}|{row.AssetType}");
                                                    }
                                                    else
                                                    {
                                                        //Most reliable method: ConID
                                                        if (false == string.IsNullOrWhiteSpace(row.ConID))
                                                        {
                                                            var securityRecord = securityRecords.FirstOrDefault(i => i.ConID == row.ConID);
                                                            if (securityRecord != null && false == string.IsNullOrWhiteSpace(securityRecord.UnderlyingSymbol))
                                                            {
                                                                instr = new Instrument(secType, securityRecord.UnderlyingSymbol, row.Currency, C2Enums.Exchange.Default, string.Empty, string.Empty, 0, 0, true);
                                                                instr.BrokerSymbol = instr.C2RootSymbol;
                                                            }
                                                        }

                                                        //Second most reliable: Symbol+AssetType
                                                        if (instr == null && false == string.IsNullOrWhiteSpace(row.Symbol) && false == string.IsNullOrWhiteSpace(row.AssetType))
                                                        {
                                                            var securityRecord = securityRecords.FirstOrDefault(i => i.Symbol == row.Symbol && i.AssetType == row.AssetType);
                                                            if (securityRecord != null && false == string.IsNullOrWhiteSpace(securityRecord.UnderlyingSymbol))
                                                            {
                                                                instr = new Instrument(secType, securityRecord.UnderlyingSymbol, row.Currency, C2Enums.Exchange.Default, string.Empty, string.Empty, 0, 0, true);
                                                                instr.BrokerSymbol = instr.C2RootSymbol;
                                                            }
                                                        }

                                                        //Third most reliable: ParseOSISymbol
                                                        if (instr == null)
                                                        {
                                                            instr = Instrument.ParseOSISymbol(entity.Symbol);
                                                            if (instr != null)
                                                            {
                                                                instr.SecurityExchange = C2Enums.Exchange.Default;
                                                                instr.Currency = row.Currency;

                                                                //Get the mapped C2 symbol
                                                                mapping = GetBrokerSymbolMapping(instr.BrokerSymbol, secType);

                                                                //ParseOSISymbol sets the BrokerSymbol but not the C2RootSymbol which must be manually set for C2FullSymbol to work correctly.
                                                                if (mapping != null)
                                                                {
                                                                    instr.C2RootSymbol = mapping.C2Symbol;
                                                                    instr.Multiplier = mapping.IBMultiplier;
                                                                }
                                                                else
                                                                {
                                                                    instr.C2RootSymbol = instr.BrokerSymbol;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                else if (entity.AssetType == IBAssetType.STK)
                                                {
                                                    instr = new Instrument(secType, row.Symbol, row.Currency, C2Enums.Exchange.Default, string.Empty, string.Empty, 0, 0, true);
                                                    instr.BrokerSymbol = instr.C2RootSymbol;

                                                    //Get the mapped C2 symbol
                                                    mapping = GetBrokerSymbolMapping(instr.BrokerSymbol, secType);

                                                    if (mapping != null)
                                                    {
                                                        instr.C2RootSymbol = mapping.C2Symbol;
                                                        instr.Multiplier = mapping.IBMultiplier;
                                                    }
                                                }
                                                else if (entity.AssetType == IBAssetType.CASH)
                                                {
                                                    instr = new Instrument(secType, row.Symbol.Replace('.', '/'), row.Currency, C2Enums.Exchange.Default, string.Empty, string.Empty, 0, 0, true);
                                                    instr.BrokerSymbol = instr.C2RootSymbol;
                                                }
                                                else if (entity.AssetType == IBAssetType.FUT)
                                                {
                                                    //Most reliable method: ConID
                                                    if (false == string.IsNullOrWhiteSpace(row.ConID))
                                                    {
                                                        var securityRecord = securityRecords.FirstOrDefault(i => i.ConID == row.ConID);
                                                        if (securityRecord != null && false == string.IsNullOrWhiteSpace(securityRecord.UnderlyingSymbol))
                                                        {
                                                            instr = new Instrument(secType, securityRecord.UnderlyingSymbol, row.Currency, C2Enums.Exchange.Default, string.Empty, string.Empty, 0, 0, true);
                                                            instr.BrokerSymbol = instr.C2RootSymbol;
                                                        }
                                                    }

                                                    //Second most reliable: Symbol+AssetType
                                                    if (instr == null && false == string.IsNullOrWhiteSpace(row.Symbol) && false == string.IsNullOrWhiteSpace(row.AssetType))
                                                    {
                                                        var securityRecord = securityRecords.FirstOrDefault(i => i.Symbol == row.Symbol && i.AssetType == row.AssetType);
                                                        if (securityRecord != null && false == string.IsNullOrWhiteSpace(securityRecord.UnderlyingSymbol))
                                                        {
                                                            instr = new Instrument(secType, securityRecord.UnderlyingSymbol, row.Currency, C2Enums.Exchange.Default, string.Empty, string.Empty, 0, 0, true);
                                                            instr.BrokerSymbol = instr.C2RootSymbol;
                                                        }
                                                    }

                                                    //Third most reliable: IBFutureSymbolParser
                                                    if (instr == null)
                                                    {
                                                        IBFutureSymbolParser parser = new IBFutureSymbolParser(row.Symbol);
                                                        instr = new Instrument(secType, parser.SymbolRoot, row.Currency, C2Enums.Exchange.Default, parser.MaturityMonthYear, string.Empty, 0, 0, true);
                                                        instr.BrokerSymbol = instr.C2RootSymbol;
                                                    }

                                                    //Get the mapped C2 symbol
                                                    mapping = GetBrokerSymbolMapping(instr.C2RootSymbol, secType);

                                                    if (mapping == null)
                                                    {
                                                        logger.Debug($"Activity: BrokerSymbol={instr.C2RootSymbol} ({secType}) is not mapped. Cannot convert {row.Symbol}");
                                                        instr = null;
                                                    }
                                                    else
                                                    {
                                                        //logger.Debug($"Mapping BrokerSymbol={mapping.IBSymbol}|C2Symbol={mapping.C2Symbol}");
                                                        instr = new Instrument(secType, mapping.C2Symbol, mapping.Currency, mapping.C2IsoExchange, instr.MaturityMonthYear, string.Empty, 0, 0, true);
                                                        instr.Multiplier = mapping.IBMultiplier;
                                                        instr.BrokerSymbol = mapping.IBSymbol;
                                                    }
                                                }
                                                else if (entity.AssetType == IBAssetType.CFD)
                                                {
                                                    //TODO: this needs to be tested
                                                    var s = row.Symbol.Split(' ');
                                                    instr = new Instrument(secType, s[0].Trim(), row.Currency, C2Enums.Exchange.Default, string.Empty, string.Empty, 0, 0, true);
                                                    instr.BrokerSymbol = instr.C2RootSymbol;
                                                }

                                                if (instr != null)
                                                {
                                                    entity.ExchangeId = instr.SecurityExchange;
                                                    entity.C2Symbol = instr.C2FullSymbol;
                                                    entity.C2PriceMultiplier = (decimal)instr.Multiplier;
                                                    if (mapping != null)
                                                    {
                                                        entity.ExchangeSymbol = mapping.ExchangeSymbol;
                                                        entity.UnitPrice *= (decimal)mapping.IBMultiplier;
                                                    }
                                                    else if (secType != QuickFix.Fields.SecurityType.FUTURE)
                                                    {
                                                        entity.ExchangeSymbol = instr.BrokerSymbol;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                logger.Debug($"Activity: AssetType is not supported: {row.Symbol}|{row.AssetType}");
                                            }
                                        }

                                        #endregion

                                        processFiles += sw.ElapsedMilliseconds;
                                        sw.Reset();

                                        sw.Start();
                                        context.Activities.Add(entity);
                                        saveToDb += sw.ElapsedMilliseconds;
                                        sw.Reset();
                                    }

                                    #endregion
                                }
                            }

                            //Save only once per file
                            if (context.ChangeTracker.HasChanges())
                            {
                                sw.Start();
                                context.SaveChanges();
                                saveToDb += sw.ElapsedMilliseconds;
                                sw.Reset();
                            }

                            #endregion

                            #region Store file in plaintext after it is processed successfully

                            if (Properties.Settings.Default.StoreDecrypted)
                            {
                                sw.Start();
                                File.WriteAllText($"{decryptedDirectoryInfo.FullName}\\{file.Name.Remove(file.Name.Length - 4)}", decryptedString, Encoding.UTF8);
                                moveFiles += sw.ElapsedMilliseconds;
                                sw.Reset();
                            }

                            #endregion

                            filesToDelete.Add(file.FullName);
                        }
                        catch (System.Data.Entity.Validation.DbEntityValidationException e)
                        {
                            #region Error log

                            StringBuilder sb = new StringBuilder();
                            foreach (var eve in e.EntityValidationErrors)
                            {
                                sb.AppendLine($"Entity of type \"{eve.Entry.Entity.GetType().Name}\" in the state \"{eve.Entry.State}\" has the following validation errors:");
                                foreach (var ve in eve.ValidationErrors)
                                    sb.AppendLine($"- Property: \"{ve.PropertyName}\", Error: \"{ve.ErrorMessage}\"");

                                foreach (var ve in eve.ValidationErrors)
                                    sb.AppendLine($"- Property: \"{ve.PropertyName}\", Value: \"{eve.Entry.CurrentValues.GetValue<object>(ve.PropertyName)}\", Error: \"{ve.ErrorMessage}\"");
                            }
                            logger.Error(e, $"Could not process File (DbEntityValidationException) '{file.Name}': {sb}");

                            #endregion
                        }
                        catch (Exception ex)
                        {
                            logger.Error(ex, $"Could not process File='{file.Name}' -> Row={currentRow}: {ex.GetInnerMostException().Message}.");
                        }
                    }
                }
            }

            //Cleanup so we don't process the files again
            sw.Start();
            foreach (var f in filesToDelete)
            {
                try
                {
                    File.Delete(f);
                }
                catch (Exception ex)
                {
                    logger.Error(ex, $"Could not delete File '{f}': {ex.GetInnerMostException().Message}");
                }
            }
            deleteFiles += sw.ElapsedMilliseconds;
            sw.Reset();

            logger.Info($"Processing {numFiles} {brokerCode.Name} files took: " +
                $"TOTAL={Utils.FormatTimeFromMS(getallFiles+decryptFiles+castRowsToObjects+processFiles+saveToDb+moveFiles+deleteFiles)} | " +
                $"GetFiles={Utils.FormatTimeFromMS(getallFiles)} | " +
                $"DecryptFiles={Utils.FormatTimeFromMS(decryptFiles)} | " +
                $"CastRows={Utils.FormatTimeFromMS(castRowsToObjects)} | " +
                $"ProcessFiles={Utils.FormatTimeFromMS(processFiles)}|" +
                $"SaveToDb={Utils.FormatTimeFromMS(saveToDb)} | " +
                $"MoveFiles={Utils.FormatTimeFromMS(moveFiles)} | " +
                $"DeleteFiles={Utils.FormatTimeFromMS(deleteFiles)}");
        }

        /// <summary>
        /// Returns the SymbolMapping object
		/// Null if not found
        /// </summary>
        /// <param name="brokerymbolRoot">The native broker root symbol</param>
        /// <param name="fixSecType">The FIX SecurityType</param>
        /// <returns></returns>
        private Database.Entities.SymbolMapping GetBrokerSymbolMapping(string brokerymbolRoot, string fixSecType)
        {
            SymbolMappingKey mappingKey = new SymbolMappingKey(brokerymbolRoot, fixSecType);
            if (false == brokerToC2Mappings.ContainsKey(mappingKey))
                return null;

            return brokerToC2Mappings[mappingKey];
        }

        private Dictionary<SymbolMappingKey, Database.Entities.SymbolMapping> LoadSymbolMappings()
        {
            var ret = new Dictionary<SymbolMappingKey, Database.Entities.SymbolMapping>();
            using (Database.Entities.Context context = new Database.Entities.Context())
            {
                foreach (var mapping in context.SymbolMappings.AsNoTracking().Where(i => i.IBMultiplier > 0).ToList())
                {
                    ret.Add(new SymbolMappingKey(mapping.IBSymbol, mapping.SecurityType), mapping);
                }
            }
            return ret;
        }

        public void MoveActivityFiles()
        {
            //Move Activity File from UnprocessedArchive to RAW.
            //Then Run() will pick them up and process them.

            var regex = new Regex("_Activity_", RegexOptions.IgnoreCase);
            var allUnprocessedActivityFiles = archiveDirectoryInfo
                .EnumerateFiles("*.csv.pgp").Where(f => regex.Match(f.Name).Success);

            int numFiles = allUnprocessedActivityFiles.Count();
            if (numFiles == 0)
            {
                logger.Info($"No {brokerCode.Name} Activity files to process");
                return;
            }
            else
            {
                logger.Info($"Processing {numFiles} Activity files for {brokerCode.Name}");
            }

            Stopwatch sw = Stopwatch.StartNew();
            foreach (var file in allUnprocessedActivityFiles)
            {
                try
                {
                    if (token.IsCancellationRequested)
                        break;

                    logger.Info($"Processing {file.Name}");

                    //Ignore everything except Activity (in case the regex above has a bug)
                    FileType fileType = Utils.GetFileType(file.Name);
                    if (fileType != FileType.Activity)
                    {
                        logger.Warn($"{file.Name} is not supported but it is in the UnprocessedArchive folder and the Regex did not filter it out. Leaving it there.");
                        continue;
                    }
                    else
                    {
                        file.MoveTo($"{activityRawDirectoryInfo.FullName}\\{file.Name}");
                    }
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException e)
                {
					#region Error log

					StringBuilder sb = new StringBuilder();
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        sb.AppendLine($"Entity of type \"{eve.Entry.Entity.GetType().Name}\" in the state \"{eve.Entry.State}\" has the following validation errors:");
                        foreach (var ve in eve.ValidationErrors)
                            sb.AppendLine($"- Property: \"{ve.PropertyName}\", Error: \"{ve.ErrorMessage}\"");

                        foreach (var ve in eve.ValidationErrors)
                            sb.AppendLine($"- Property: \"{ve.PropertyName}\", Value: \"{eve.Entry.CurrentValues.GetValue<object>(ve.PropertyName)}\", Error: \"{ve.ErrorMessage}\"");
                    }
                    logger.Error(e, $"Could not process File '{file.Name}': {sb.ToString()}");

					#endregion
				}
				catch (Exception ex)
                {
                    logger.Error(ex, $"Could not process File '{file.Name}': {ex.GetInnerMostException().Message}");
                }
            }

            logger.Info($"Processing {numFiles} {brokerCode.Name} files took {Utils.FormatTime(sw)}");

        }

        /// <summary>
        /// Hack to remap the BrokerCode if the FTP is the same as C2
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="currentCode"></param>
        /// <returns></returns>
        private int GetBrokerCodeFromFileName(string fileName, int currentCode)
        {
            if (currentCode == BrokerCode.C2 && fileName.StartsWith("F1867187", StringComparison.OrdinalIgnoreCase))
                return BrokerCode.IB_Israel;
            else
                return currentCode;
        }

        private static decimal ParseDecimal(string val)
        {
            if (decimal.TryParse(val, out decimal cash))
                return cash;
            else
                return 0;
        }

        /// <summary>
        /// Expecting input in format U481497_NAV_20190822.csv
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private DateTime GetDateFromFileName(string name)
        {
            var s = Path.GetFileNameWithoutExtension(name).Split('_');
            if (s.Length < 3)
                throw new Exception($"GetDateFromFileName cannot parse date from {name}. We expect format 'U481497_NAV_20190822.csv'");

            if (false == DateTime.TryParseExact(s[2], "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime ParsedDate))
                throw new Exception($"GetDateFromFileName cannot parse date from {s[2]}. We expect format '20190822'");

            return ParsedDate;
        }       
    }
}
