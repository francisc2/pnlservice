﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace PnLService.ServiceInterface
{
    public static class Utils
    {
        public static FileType GetFileType(string name)
        {
            name = name.ToUpperInvariant();
            
            if (name.Contains("_ACCOUNT_"))
                return FileType.Account;
            else if (name.Contains("_ACTIVITY_"))
                return FileType.Activity;
            else if (name.Contains("_CASHREPORT_"))
                return FileType.CashReport;
            else if (name.Contains("_NAV_"))
                return FileType.NAV;
            else if (name.Contains("_PL_"))
                return FileType.PnL;
            else if (name.Contains("_POSITION_"))
                return FileType.Position;
            else if (name.Contains("_SECURITY_"))
                return FileType.Security;
            else
                return FileType.Unknown;
        }

        public static string FormatTime(Stopwatch sw)
        {
            int hours = (int)Math.Truncate(sw.Elapsed.TotalMinutes / 60);
            int minutes = (int)sw.Elapsed.TotalMinutes;
            double fsec = 60 * (sw.Elapsed.TotalMinutes - minutes);
            int sec = (int)fsec;
            int ms = (int)Math.Truncate(1000 * (fsec - sec));
            string tsOut = $"{hours:D2}:{minutes:D2}:{sec:D2}.{ms}";
            return tsOut;
        }

        public static string FormatTimeFromMS(long milliSeconds)
        {
            long hours = milliSeconds / 1000 / 60 / 60;
            long minutes = milliSeconds / 1000 / 60;
            long sec = milliSeconds / 1000;
            return $"{hours:D2}:{minutes:D2}:{sec:D2}.{milliSeconds}";
        }

        public static string  IBAssetTypeToFIXSecurityType(string ibAssetType)
        {
            switch (ibAssetType)
            {
                case IBAssetType.STK:
                case IBAssetType.CFD:
                    return QuickFix.Fields.SecurityType.COMMON_STOCK;
                case IBAssetType.OPT:
                    return QuickFix.Fields.SecurityType.OPTION;
                case IBAssetType.FUT:
                    return QuickFix.Fields.SecurityType.FUTURE;
                case IBAssetType.CASH:
                    return QuickFix.Fields.SecurityType.FOREIGN_EXCHANGE_CONTRACT;
                default:
                    return null;
            }
        }
    }
}
