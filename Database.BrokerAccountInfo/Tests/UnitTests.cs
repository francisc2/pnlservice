﻿using System;
using NUnit.Framework;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using Database.BrokerAccountInfo.Entities;
using System.Linq.Expressions;
using System.Text;

namespace Database.BrokerAccountInfo.Tests
{
    [TestFixture]
    public class UnitTests
    {
        public UnitTests()
        {
        }

        [TearDown]
        public void TestFixtureTearDown()
        {
        }

        [TestCase("Database.BrokerAccountInfo.Entities", @"name=BrokerAccountInfo")]
        public void Test_DBContext(string TargetNamespace, string ConnectionStr)
        {
            List<string> messages = new List<string>();

			List<MethodInfo> countMethods = typeof(Queryable)
                .GetMethods(BindingFlags.Static | BindingFlags.Public)
                .Where(mi => mi.Name == "Count" && mi.GetParameters().Length == 1)
                .ToList();

			List<MethodInfo> lambdaMethods = typeof(Expression).GetMethods(BindingFlags.Public | BindingFlags.Static)
                .Where(mi => mi.Name.StartsWith("Lambda") && mi.GetGenericArguments().Length == 1)
                .ToList();

			List<MethodInfo> whereMethods = typeof(Queryable).GetMethods(BindingFlags.Public | BindingFlags.Static)
                .Where(mi => mi.Name.StartsWith("Where") && mi.GetParameters()[1].ParameterType.GetGenericArguments()[0].GetGenericArguments().Length == 2)
                .ToList();

			List<MethodInfo> firstMethods = typeof(Queryable).GetMethods(BindingFlags.Public | BindingFlags.Static)
                .Where(mi => mi.Name == "First" && mi.GetParameters().Length == 1)
                .ToList();

            using (Context dbContextUnderTest = new Context(ConnectionStr))
            {
                List<Tuple<PropertyInfo, Type, Type>> entitiesProperties = GetAllEntitiesProperties(dbContextUnderTest, TargetNamespace);
                foreach (var entry in entitiesProperties)
                {
                    object dbSet = entry.Item1.GetGetMethod().Invoke(dbContextUnderTest, new object[] { });

                    #region Count
                    int count = 0;
                    try
                    {

                        MethodInfo countMethod = countMethods.First().MakeGenericMethod(entry.Item3);
                        count = (int)countMethod.Invoke(dbSet, new object[] { dbSet });
                    }
                    catch (Exception Ex)
                    {
                        count = -1;
                        messages.Add($"Count test failed for property {entry.Item1.Name} of type {entry.Item2}. No create/read/delete tests will be performed on this set. Reason = {Ex.ToString()}.");
                    }
                    #endregion

                    if (count > 0)
                    {
                        #region Read Test

                        try
                        {

                            var param = Expression.Parameter(entry.Item3, "p");
                            var righHand = Expression.Constant(true);

                            var fncType = typeof(Func<,>);
                            var actualFncType = fncType.MakeGenericType(entry.Item3, typeof(bool));

                            var lambdaMethod = lambdaMethods.First().MakeGenericMethod(actualFncType);

                            var expresion = lambdaMethod.Invoke(null, new object[] { righHand, new ParameterExpression[] { param } });

                            var whereMethod = whereMethods.First().MakeGenericMethod(entry.Item3);

                            var firstMethod = firstMethods.First().MakeGenericMethod(entry.Item3);

                            var result = firstMethod.Invoke(null, new object[] { whereMethod.Invoke(null, new object[] { dbSet, expresion }) });

                            var propResult = entry.Item3.GetProperties(BindingFlags.Public | BindingFlags.Instance);
                            foreach (var prop in propResult)
                            {
                                try
                                {
                                    var obj = prop.GetGetMethod().Invoke(result, new object[] { });
                                }
                                catch (Exception Ex)
                                {
                                    throw new Exception($"Unable to read property {prop.Name} of {entry.Item3}.", Ex);
                                }
                            }
                        }
                        catch (Exception Ex)
                        {
                            messages.Add($"Read test failed for property {entry.Item1.Name} of type {entry.Item2}. Reason = {Ex.ToString()}.");
                        }

                        #endregion
                    }
                }
            }

            StringBuilder sb = new StringBuilder();

            sb.AppendLine($"There were {messages.Count} errors.");

            foreach (var message in messages)
                sb.AppendLine($"{message.Substring(0, 1000)}(...)");

			string errorText = sb.ToString();

            if (messages.Count == 0)
                Assert.Pass();
            else
                throw new ApplicationException(errorText);

        }

        private static List<Tuple<PropertyInfo, Type, Type>> GetAllEntitiesProperties(object dbContext, string TargetNamespace)
        {
            List<Tuple<PropertyInfo, Type, Type>> result = new List<Tuple<PropertyInfo, Type, Type>>();
			Type baseType = dbContext.GetType();
			PropertyInfo[] allProps = baseType.GetProperties(BindingFlags.Public | BindingFlags.DeclaredOnly | BindingFlags.Instance);
            
            foreach(var property in allProps)
            {
                if(property.PropertyType.Name.StartsWith("DbSet"))
                {
                    var propType = property.PropertyType;
                    if (!propType.IsConstructedGenericType)
                        continue;

                    Type[] genParams = propType.GetGenericArguments();
                    if (genParams.Length != 1)
                        continue;

                    if (genParams[0].Namespace != TargetNamespace)
                        continue;

                    result.Add(new Tuple<PropertyInfo, Type, Type>(property, propType, genParams[0]));
                }
            }

            return result;
        }
    }
}