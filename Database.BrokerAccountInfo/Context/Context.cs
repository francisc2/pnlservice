﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.Data.Entity.Core;
using System.Data.Entity.Core.EntityClient;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.Objects.DataClasses;
using System.CodeDom.Compiler;

namespace Database.BrokerAccountInfo.Entities
{

    public partial class Context
    {
        //private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        ///// <summary>
        ///// Refresh underlying data before applying the update if it fails
        ///// </summary>
        ///// <param name="refreshMode"></param>
        ///// <returns></returns>
        //public int SaveChanges(RefreshMode refreshMode)
        //{
        //    try
        //    {
        //        int ret = base.SaveChanges();
        //        return ret;
        //    } 
        //    catch (DbUpdateException ex)
        //    {
        //        foreach (DbEntityEntry entry in ex.Entries)
        //        {
        //            if (refreshMode == RefreshMode.ClientWins)
        //            {
        //                if (entry.State != EntityState.Modified)
        //                    entry.State = EntityState.Modified;

        //                logger.Debug($"GetDatabaseValues for {entry.Entity} (state={entry.State})");
        //                entry.OriginalValues.SetValues(entry.GetDatabaseValues());
        //            }
        //            else
        //            {
                        
        //                logger.Debug($"Reload {entry.Entity} (state={entry.State})");
        //                entry.Reload();
        //            }
        //        }
        //        return base.SaveChanges();
        //    }
        //}
    }
}
