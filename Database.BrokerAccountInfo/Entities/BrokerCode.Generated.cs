﻿//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Devart Entity Developer tool using Entity Framework DbContext template.
// Code is generated on: 2023-08-03 6:59:46 PM
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations;

namespace Database.BrokerAccountInfo.Entities
{
    [GeneratedCode("Devart Entity Developer", "")]
    public partial class BrokerCode : ICloneable
    {

        public BrokerCode()
        {
            this.Code = 0;
            this.Name = @"";
            this.Enabled = true;
            this.Host = @"";
            this.Username = @"";
            this.Password = @"";
            this.KeyPassphrase = @"";
            this.KeyFilename = @"";
            OnCreated();
        }

        #region Properties

        [Key]
        [Required()]
        public virtual long Id { get; set; }

        [Required()]
        public virtual int Code { get; set; }

        [StringLength(255)]
        [Required()]
        public virtual string Name { get; set; }

        [Required()]
        public virtual bool Enabled { get; set; }

        [StringLength(255)]
        [Required()]
        public virtual string Host { get; set; }

        [StringLength(255)]
        [Required()]
        public virtual string Username { get; set; }

        [StringLength(255)]
        [Required()]
        public virtual string Password { get; set; }

        [StringLength(255)]
        [Required()]
        public virtual string KeyPassphrase { get; set; }

        [StringLength(255)]
        [Required()]
        public virtual string KeyFilename { get; set; }

        #endregion

        #region Extensibility Method Definitions

        partial void OnCreated();

        #endregion

        #region ICloneable Members

        public virtual object Clone()
        {
            BrokerCode obj = new BrokerCode();
            obj.Id = Id;
            obj.Code = Code;
            obj.Name = Name;
            obj.Enabled = Enabled;
            obj.Host = Host;
            obj.Username = Username;
            obj.Password = Password;
            obj.KeyPassphrase = KeyPassphrase;
            obj.KeyFilename = KeyFilename;
            return obj;
        }

        #endregion

        #region Equals and GetHashCode methods

        public override bool Equals(object obj)
        {
          BrokerCode toCompare = obj as BrokerCode;
          if (toCompare == null)
          {
            return false;
          }

          if (!Object.Equals(this.Id, toCompare.Id))
            return false;

          return true;
        }

        public override int GetHashCode()
        {
          int hashCode = 13;
          hashCode = (hashCode * 7) + Id.GetHashCode();
          return hashCode;
        }

        #endregion
    }

}
