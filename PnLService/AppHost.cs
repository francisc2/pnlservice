﻿using System;
using System.Collections.Generic;
using Funq;
using ServiceStack;
using ServiceStack.Text;
using PnLService.ServiceInterface;
using ServiceStack.Formats;

namespace PnLService
{
    //VS.NET Template Info: https://servicestack.net/vs-templates/EmptyWindowService
    public class AppHost : AppSelfHostBase
    {
        static NLog.Logger logger = NLog.LogManager.GetLogger("AppHost");
        System.Timers.Timer timer = null;

        /// <summary>
        /// Base constructor requires a Name and Assembly where web service implementation is located
        /// </summary>
        public AppHost()
            : base("PnLService", typeof(APIServices).Assembly)
        {
            logger.Info($"Initializing PnLService v{Build(System.Reflection.Assembly.GetAssembly(this.GetType()))} on {Properties.Settings.Default.Host}:{Properties.Settings.Default.Port}");
        }


        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                logger.Info("Start Timer_Elapsed");
                timer.Stop();               
                base.ExecuteService(new PnLService.ServiceModel.DownloadFiles());
                base.ExecuteService(new PnLService.ServiceModel.ProcessFiles());

                //Now handled by APIv4
                //if (DateTime.Now.DayOfWeek != DayOfWeek.Saturday && DateTime.Now.DayOfWeek != DayOfWeek.Sunday)
                //    base.ExecuteService(new PnLService.ServiceModel.GetAutotradeStatsSnapshot(true));
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"PnLService error while running request: {ex.Message}");
            }
            finally
            {
                logger.Info("End Timer_Elapsed");
                timer.Interval = MillisecondsToNextRun();
                timer.Start();
            }
        }

        /// <summary>
        /// Application specific configuration
        /// This method should initialize any IoC resources utilized by your web service classes.
        /// </summary>
        public override void Configure(Container container)
        {
            JsConfig.TextCase = TextCase.Default;
            JsConfig.AssumeUtc = true;
            JsConfig.AlwaysUseUtc = true;
            JsConfig.ExcludeDefaultValues = true;
            JsConfig.DateHandler = DateHandler.ISO8601;
            JsConfig.ExcludeTypeInfo = true; //Hide _type descriptor

            SetConfig(new HostConfig
            {
                DebugMode = System.Diagnostics.Debugger.IsAttached, //Show StackTraces in service responses during development
                WriteErrorsToResponse = true,
                DefaultContentType = MimeTypes.Json,
                AllowSessionIdsInHttpParams = true,
                AllowJsonpRequests = true,
                ApiVersion = "1.0",
                MetadataVisibility = RequestAttributes.Any,
                GlobalResponseHeaders = new Dictionary<string, string>
                {
                    { "Vary", "Accept" },
                    { "X-Powered-By", "Collective2" },
                },
            });

            Plugins.Add(new CancellableRequestsFeature());

            #region Install custom CSV

            Plugins.RemoveAll(x => x is CsvFormat);
            Plugins.Add(new CustomCsvFormat());

            #endregion

            #region Request logging

            Plugins.Add(new RequestLogsFeature
            {
                EnableErrorTracking = true,
                EnableSessionTracking = true,
                EnableRequestBodyTracking = true,
                LimitToServiceRequests = false,
                RequestLogger = new RequestLogger()
            });

            #endregion

            #region Postman

            Plugins.Add(new PostmanFeature());
            Plugins.Add(new CorsFeature());

            #endregion
        }

        /// <summary>
		/// Returns the build number for display
		/// </summary>
		/// <returns></returns>
		internal static string Build(System.Reflection.Assembly assembly)
		{
			Version v = assembly.GetName().Version;
			string ret;
			ret = $"{v.Major}.{v.Minor}.{v.Build}";
			return ret;
		}

        public override ServiceStackHost Start(string urlBase)
        {
            timer = new System.Timers.Timer(MillisecondsToNextRun());
            timer.Elapsed += Timer_Elapsed;
            timer.Start();
            logger.Info("Autorun Timer started");
            return base.Start(urlBase);
        }

        public override void Stop()
        {
            if (timer != null)
            {
                timer.Stop();
                timer = null;
            }
            base.Stop();
        }

        /// <summary>
        /// Every day 8AM
        /// </summary>
        /// <returns></returns>
        private double MillisecondsToNextRun()
		{
			TimeSpan day = new TimeSpan(24, 00, 00);    //24 hours in a day.
			TimeSpan now = TimeSpan.Parse(DateTime.Now.ToString("HH:mm:ss"));     //The current time in 24 hour format
			TimeSpan activationTime = new TimeSpan(9, 15, 0);    //9:15AM

			TimeSpan timeLeftUntilFirstRun = ((day - now) + activationTime);
			if(timeLeftUntilFirstRun.TotalHours > 24)
				timeLeftUntilFirstRun -= new TimeSpan(24, 0, 0);    //Deducts a day from the schedule so it will run today.

			logger.Info($"Timer set to trigger in {timeLeftUntilFirstRun.ToString("T")} (at {activationTime.ToString("T")})");

			return timeLeftUntilFirstRun.TotalMilliseconds;
		}
    }
}
