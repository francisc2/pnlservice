﻿using System.ComponentModel;
using System.Configuration.Install;

namespace PnLService
{
    [RunInstaller(true)]
    public partial class WinServiceInstaller : Installer
    {
        public WinServiceInstaller()
        {
            InitializeComponent();
        }
    }
}
