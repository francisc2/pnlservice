﻿using System;
using System.IO;
using ServiceStack;
using ServiceStack.Text;

namespace PnLService
{
	class CustomCsvFormat : IPlugin
	{
		public void Register(IAppHost appHost)
		{
			//Register the 'text/csv' content-type and serializers (format is inferred from the last part of the content-type)
			appHost.ContentTypes.Register(C2Enums.MimeTypes.Text.Csv, SerializeToStream, CsvSerializer.DeserializeFromStream);

            appHost.GlobalResponseFilters.Add((req, res, dto) =>
            {
                if (req.ResponseContentType == C2Enums.MimeTypes.Text.Csv)
                {
                    string csvFilename = req.OperationName;

                    // look for custom csv-filename set from Service code
                    if ((string)req.GetItem("csv-filename") != default)
                    {
                        csvFilename = (string)req.GetItem("csv-filename");
                    }

                    res.AddHeader(HttpHeaders.ContentDisposition, $"attachment;filename={csvFilename}.csv");
                }
            });
        }

        public void SerializeToStream(ServiceStack.Web.IRequest requestContext, object request, Stream stream)
        {
            if (request is ServiceModel.CMEAuditResponse)
                CsvSerializer.SerializeToStream(((ServiceModel.CMEAuditResponse)request).Log, stream);
            else
                CsvSerializer.SerializeToStream(request, stream);
        }
    }
}
