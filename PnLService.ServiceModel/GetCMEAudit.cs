﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Data.Entity;
using ServiceStack;
using C2Enums;
using Database.Entities;
using System.IO;
using System.Text.RegularExpressions;
using System.Globalization;
using Org.BouncyCastle.Asn1.Ocsp;
using System.Collections;

namespace PnLService.ServiceModel
{
    [Api]
    [Route("/cmeaudit", "GET", Summary = @"Returns CME Audit trail for the Account. You must also provide OrderId and/or SignalId. If no Account is set, all accounts that traded the SignalId will be returned")]
    public class CMEAudit: IReturn<CMEAuditResponse>
    {
        private static NLog.Logger logger = NLog.LogManager.GetLogger("GetCMEAudit");

        #region Parameters

        [ApiMember(Description = @"Account", IsRequired = false)]
        public string Account { get; set; }


        [ApiMember(Description = @"The Broker (Not CME) OrderId. Exact match only", IsRequired = false)]
        public string OrderId { get; set; }


        [ApiMember(Description = @"C2 SignalId", IsRequired = false)]
        public int SignalId { get; set; }


        [ApiMember(Description = @"Symbol. Optional, only used for file name", IsRequired = false)]
        public string Symbol { get; set; }


        [ApiMember(Description = @"The FCM Account. Optional, only used for file name", IsRequired = false)]
        public string FCMAccount { get; set; }

        #endregion

        public object Get(CancellationToken token, string account, string orderId, int signalId)
        {
            logger.Debug($"{this.GetType().Name}.Get: {this.ToSafeJson()}");

            List<Database.Entities.AuditLog> list = null;

            #region Validation

            if (string.IsNullOrWhiteSpace(orderId) && signalId < 1)
            {
                throw new MissingFieldException("You must provide either the OrderId or the SignalId (or both)");
            }
            else if (string.IsNullOrWhiteSpace(account) && signalId < 1)
            {
                throw new MissingFieldException("You must provide the SignalId. OrderId is not unique and can result in invalid data");
            }

            #endregion

            #region Get the data

            using (Database.Entities.Context context = new Database.Entities.Context())
            {
                if (signalId < 1)
                {
                    //Get it from the order
                    var orders = context.Orders.Where(i => i.AccountName == account && i.OrderId == OrderId).ToList();
                    if (orders.Count > 1)
                    {
                        throw new InvalidDataException($"The database contains more than one OrderId {orderId} for account {account}. Cannot trust results. Contact Francis, we'll probably need a StartDate parameter");
                    }
                    else if (orders.Count > 0)
                    {
                        signalId = orders[0].SignalId;
                    }
                }

                if (signalId < 1)
                {
                    list = context.AuditLogs.Where(i => i.AccountName == account
                    && i.OrderId == orderId
                    && (i.MsgType == AuditMessageType.C2Signal || i.MsgType == AuditMessageType.Order || i.MsgType == AuditMessageType.Execution || i.MsgType == AuditMessageType.C2Cancel)
                    && false == i.Message.Contains("PENDING_CANCEL")
                    && false == i.Message.Contains("RISK_SYSTEM")
                    && false == i.Message.Contains("Converting OrderId")
                    && false == i.Message.Contains("PENDING_NEW")
                    && false == i.Message.Contains("Status=FILLED|OrderId")
                    && false == i.Message.Contains("Status=PARTIALLY_FILLED|OrderId")
                    && false == i.Message.Contains("REJECTED")
                    && false == i.Message.Contains("Canceling OrderID")
                    && false == i.Message.Contains("Sending new C2")
                    ).ToList();
                }
                else if (false == string.IsNullOrWhiteSpace(orderId) && signalId > 0)
                {
                    if (string.IsNullOrWhiteSpace(account))
                    {
                        list = context.AuditLogs.Where(i => i.OrderId == orderId
                        && i.SignalId == signalId
                        && (i.MsgType == AuditMessageType.C2Signal || i.MsgType == AuditMessageType.Order || i.MsgType == AuditMessageType.Execution || i.MsgType == AuditMessageType.C2Cancel)
                        && false == i.Message.Contains("PENDING_CANCEL")
                        && false == i.Message.Contains("RISK_SYSTEM")
                        && false == i.Message.Contains("Converting OrderId")
                        && false == i.Message.Contains("PENDING_NEW")
                        && false == i.Message.Contains("Status=FILLED|OrderId")
                        && false == i.Message.Contains("REJECTED")
                        && false == i.Message.Contains("Status=PARTIALLY_FILLED|OrderId")
                        && false == i.Message.Contains("Canceling OrderID")
                        && false == i.Message.Contains("Sending new C2")
                        ).ToList();
                    }
                    else
                    {
                        list = context.AuditLogs.Where(i => i.AccountName == account
                        && i.OrderId == orderId
                        && i.SignalId == signalId
                        && (i.MsgType == AuditMessageType.C2Signal || i.MsgType == AuditMessageType.Order || i.MsgType == AuditMessageType.Execution || i.MsgType == AuditMessageType.C2Cancel)
                        && false == i.Message.Contains("PENDING_CANCEL")
                        && false == i.Message.Contains("RISK_SYSTEM")
                        && false == i.Message.Contains("Converting OrderId")
                        && false == i.Message.Contains("PENDING_NEW")
                        && false == i.Message.Contains("Status=FILLED|OrderId")
                        && false == i.Message.Contains("REJECTED")
                        && false == i.Message.Contains("Status=PARTIALLY_FILLED|OrderId")
                        && false == i.Message.Contains("Canceling OrderID")
                        && false == i.Message.Contains("Sending new C2")
                        ).ToList();
                    }
                }
                else if (string.IsNullOrWhiteSpace(orderId) && signalId > 0)
                {
                    if (string.IsNullOrWhiteSpace(account))
                    {
                        list = context.AuditLogs.Where(i => i.SignalId == signalId
                        && i.BrokerId != BrokerId.PaperTrade
                        && (i.MsgType == AuditMessageType.C2Signal || i.MsgType == AuditMessageType.Order || i.MsgType == AuditMessageType.Execution || i.MsgType == AuditMessageType.C2Cancel)
                        && false == i.Message.Contains("PENDING_CANCEL")
                        && false == i.Message.Contains("RISK_SYSTEM")
                        && false == i.Message.Contains("Converting OrderId")
                        && false == i.Message.Contains("PENDING_NEW")
                        && false == i.Message.Contains("Status=FILLED|OrderId")
                        && false == i.Message.Contains("REJECTED")
                        && false == i.Message.Contains("Status=PARTIALLY_FILLED|OrderId")
                        && false == i.Message.Contains("Canceling OrderID")
                        && false == i.Message.Contains("Sending new C2")
                        ).ToList();
                    }
                    else
                    {
                        list = context.AuditLogs.Where(i => i.AccountName == account
                        && i.SignalId == signalId
                        && (i.MsgType == AuditMessageType.C2Signal || i.MsgType == AuditMessageType.Order || i.MsgType == AuditMessageType.Execution || i.MsgType == AuditMessageType.C2Cancel)
                        && false == i.Message.Contains("PENDING_CANCEL")
                        && false == i.Message.Contains("RISK_SYSTEM")
                        && false == i.Message.Contains("Converting OrderId")
                        && false == i.Message.Contains("PENDING_NEW")
                        && false == i.Message.Contains("Status=FILLED|OrderId")
                        && false == i.Message.Contains("REJECTED")
                        && false == i.Message.Contains("Status=PARTIALLY_FILLED|OrderId")
                        && false == i.Message.Contains("Canceling OrderID")
                        && false == i.Message.Contains("Sending new C2")
                            ).ToList();
                    }
                }
                else if (false == string.IsNullOrWhiteSpace(orderId) && signalId < 1)
                {
                    list = context.AuditLogs.Where(i => i.AccountName == account
                    && i.OrderId == orderId
                    && (i.MsgType == AuditMessageType.C2Signal || i.MsgType == AuditMessageType.Order || i.MsgType == AuditMessageType.Execution || i.MsgType == AuditMessageType.C2Cancel)
                    && false == i.Message.Contains("PENDING_CANCEL")
                    && false == i.Message.Contains("RISK_SYSTEM")
                    && false == i.Message.Contains("Converting OrderId")
                    && false == i.Message.Contains("PENDING_NEW")
                    && false == i.Message.Contains("Status=FILLED|OrderId")
                    && false == i.Message.Contains("REJECTED")
                    && false == i.Message.Contains("Status=PARTIALLY_FILLED|OrderId")
                    && false == i.Message.Contains("Canceling OrderID")
                    && false == i.Message.Contains("Sending new C2")
                    ).ToList();
                }
                else
                {
                    throw new InvalidOperationException("WTF parameter confusion");
                }
            }

            #endregion

            

            return new CMEAuditResponse()
            {
                Log = FormatAuditLogForCME(list),
            };
        }

        List<CMEAuditDTO> FormatAuditLogForCME(List<Database.Entities.AuditLog> list)
        {
            TimeZoneInfo tz = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
            List<CMEAuditDTO> ret = new List<CMEAuditDTO>();
            foreach (var item in list)
            {
                CMEAuditDTO newItem = new CMEAuditDTO { Account = item.AccountName, MsgType = Format(item.MsgType), OrderId = item.OrderId, SignalId = item.SignalId, SystemId = item.SystemId, Timestamp = TimeZoneInfo.ConvertTimeFromUtc(item.Timestamp, tz).ToString("G"), Message = item.Message };
                newItem.Message = newItem.Message.Replace("System Signal", "Order Created by Strategy. Signal");
                newItem.Message = newItem.Message.Replace("RiskManager", "Rule 575 Monitor");
                newItem.Message = newItem.Message.Replace("Status=NEW|", "Order sent to broker. ");
                newItem.Message = newItem.Message.Replace("Sending new OS Order", "Creating new Order:");
                newItem.Message = newItem.Message.Replace("so we're holding order", ". Cannot send order");
                newItem.Message = newItem.Message.Replace("BidSize", "TopOfBookBidSize");
                newItem.Message = newItem.Message.Replace("AskSize", "TopOfBookAskSize");
                newItem.Message = newItem.Message.Replace(" Canceling because converted to a market order.", "Converting to a market order because strategy requires immediate synchronization with position in Model Account");
                newItem.Message = newItem.Message.Replace("so we're sending order as-is", "so sending order as-is");
                newItem.Message = newItem.Message.Replace(": OrderQty", ": Checking if market liquidity is sufficient to send order? Yes: OrderQty");
                newItem.Message = newItem.Message.Replace("Order sent to broker. OrderId ", "OrderId ");
                newItem.Message = newItem.Message.Replace("order as-is:", "order as-is. Order Sent to Broker. ");
                var split = newItem.Message.SplitOnFirst("|Timestamp=");
                newItem.Message = split[0];
                split = newItem.Message.SplitOnLast("ProcessTime=");
                newItem.Message = split[0];
                ret.Add(newItem);
            }
            return ret;
        }

        static String LikeToRegular(String value)
        {
            return "^" + Regex.Escape(value).Replace("_", ".").Replace("%", ".*") + "$";
        }

        static string Format(AuditMessageType msgType)
        {
            switch (msgType)
            {
                case AuditMessageType.C2Signal:
                    return "Strategy Signal";
                case AuditMessageType.Execution:
                    return "Execution";
                case AuditMessageType.Order:
                    return "Order";
                case AuditMessageType.StopLoss:
                    return "StopLoss";
                case AuditMessageType.C2Cancel:
                    return "Strategy Cancel";
                case AuditMessageType.C2Fill:
                    return "Strategy Fill";
                default:
                    return "";
            }
        }
    }

    [ApiResponse]
    public class CMEAuditResponse: BaseResponse
    {
        [ApiMember(Description = @"CME Audit data", IsRequired = true)]
        public List<CMEAuditDTO> Log { get; set; } = new List<CMEAuditDTO>();
    }
}
