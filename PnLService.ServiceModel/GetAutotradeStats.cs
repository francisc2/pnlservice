﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Data.Entity;
using ServiceStack;
using C2Enums;

namespace PnLService.ServiceModel
{
    [Api]
    [Route("/getautotradestats", "GET", Summary = @"Returns the Autotrade stats for all brokers")]
    public class GetAutotradeStats : IReturn<GetAutotradeStatsResponse>
    {
        private bool savedata;
        private static NLog.Logger logger = NLog.LogManager.GetLogger("GetAutotradeStats");

        public GetAutotradeStats(bool savedata = false)
        {
            this.savedata = savedata;
        }

        [ApiMember(Description = @"StartDate", IsRequired = false)]
        public DateTime? StartDate  { get; set; }


        [ApiMember(Description = @"EndDate", IsRequired = false)]
        public DateTime? EndDate  { get; set; }


        public object Get(CancellationToken token)
        {
            logger.Debug($"{this.GetType().Name}.Get: {this.ToSafeJson()}");


            List<Database.Statistics.Entities.Autotrade> list = null;
            using (Database.Statistics.Entities.Context context = new Database.Statistics.Entities.Context())
            {
                list = context.Autotrades.Where(i => i.Utcdate >= StartDate && i.Utcdate <= EndDate).Take(2000).ToList();
            }

            return new GetAutotradeStatsResponse()
            {
                Stats = list,
            };
        }
    }


    [ApiResponse]
    public class GetAutotradeStatsResponse: BaseResponse
    {
        [ApiMember(Description = @"Autotrade stats", IsRequired = true)]
        public List<Database.Statistics.Entities.Autotrade> Stats { get; set; }
    }
}
