﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Data.Entity;
using ServiceStack;
using C2Enums;

namespace PnLService.ServiceModel
{
    [Api]
    [Route("/getautotradestatssnapshot", "GET", Summary = @"Returns the Autotrade stats for all brokers")]
    [Route("/getautotradestatssnapshot/{BrokerId}", "GET", Summary = @"Returns the Autotrade stats for the BrokerId")]
    public class GetAutotradeStatsSnapshot : IReturn<GetAutotradeStatsSnapshotResponse>
    {
        private bool savedata;
        private static NLog.Logger logger = NLog.LogManager.GetLogger("GetAutotradeStats");

        public GetAutotradeStatsSnapshot(bool savedata = false)
        {
            this.savedata = savedata;
        }

        [ApiMember(Description = @"BrokerID", IsRequired = false)]
        public int BrokerID { get; set; }


        public object Get(CancellationToken token, BrokerId brokerId)
        {
            logger.Debug($"{this.GetType().Name}.Get: {this.ToSafeJson()}");

            var stats = new AutotradeStatsDTO();
            stats.UTCDate = DateTime.UtcNow;

            DateTime utc7DaysAgo = DateTime.UtcNow.AddDays(-7);
            DateTime utc30DaysAgo = DateTime.UtcNow.AddDays(-30);
            DateTime utc60DaysAgo = DateTime.UtcNow.AddDays(-60);
            DateTime utc90DaysAgo = DateTime.UtcNow.AddDays(-90);

            using (Database.Entities.Context context = new Database.Entities.Context())
            {
                #region C2Systems

                logger.Debug("Start C2Systems queries");

                var allSystemsQuery = $"SELECT a.UserId, s.AccountName, a.BrokerId, s.SystemId, s.ForexPercent, s.FuturesPercent, s.StocksPercent, s.OptionsPercent, s.IsPublisher, a.NetLiquidationValue " +
                    "FROM accounts a, systems s " +
                    "WHERE a.BrokerActive " +
                    "AND s.AccountName = a.Name AND s.UserId = a.UserId AND a.BrokerId = s.BrokerId " +
                    "AND (s.ForexPercent > 0 || s.FuturesPercent > 0 || s.StocksPercent > 0 || s.OptionsPercent > 0) " +
                    "AND a.C2Active";

                if (this.BrokerID > 0)
                    allSystemsQuery += $" AND a.BrokerId = {this.BrokerID}";
                else
                    allSystemsQuery += $" AND a.BrokerId <> {(int)BrokerId.Demo} AND a.BrokerId <> {(int)BrokerId.PaperTrade}";

                #region MUCH slower code. Also much easier to maintain :-(

                //var query = from a in context.AccountDBOs
                //            from s in context.C2Systems
                //            where a.BrokerActive && a.C2Active && a.BrokerId != BrokerId.Demo && a.BrokerId != BrokerId.PaperTrade
                //            && s.AccountName == a.Name
                //            && s.UserId == a.UserId
                //            && (s.ForexPercent > 0 || s.FuturesPercent > 0 || s.StocksPercent > 0 || s.OptionsPercent > 0)
                //            && s.BrokerId != BrokerId.Demo && s.BrokerId != BrokerId.PaperTrade
                //            select new { s.UserId, s.AccountName, s.BrokerId, s.SystemId, s.ForexPercent, s.FuturesPercent, s.StocksPercent, s.OptionsPercent, s.IsPublisher };

                //var allSystems = query.ToList();

                #endregion

                var allSystemsSqlQuery = context.Database.SqlQuery<S>(allSystemsQuery, new object[] { });
                var allSystems = allSystemsSqlQuery.ToList();
                var atSystems = allSystems.Where(i => false == i.IsPublisher).ToList();
                var btSystems = allSystems.Where(i => i.IsPublisher).ToList();

                List<string> processed = new List<string>();
                double netLiqu = 0;
                foreach (var item in allSystems)
                {
                    if (false == processed.Contains(item.AccountName))
                    {
                        processed.Add(item.AccountName);
                        netLiqu += item.NetLiquidationValue;
                    }
                }

                stats.Accounts.Autotrading_Accounts = atSystems.GroupBy(g => g.AccountName).ToList().Count;
                stats.Accounts.BrokerTransmit_Accounts = btSystems.GroupBy(g => g.AccountName).ToList().Count;
                stats.Accounts.AUM = (int)netLiqu;

                stats.Strategies.Total_Autotraded = atSystems.Count;
                stats.Strategies.Total_Autotraded_Forex = atSystems.Where(i => i.ForexPercent > 0).ToList().Count;
                stats.Strategies.Total_Autotraded_Futures = atSystems.Where(i => i.FuturesPercent > 0).ToList().Count;
                stats.Strategies.Total_Autotraded_Stocks = atSystems.Where(i => i.StocksPercent > 0).ToList().Count;
                stats.Strategies.Total_Autotraded_Options = atSystems.Where(i => i.OptionsPercent > 0).ToList().Count;

                stats.Strategies.Unique_Autotraded = atSystems.GroupBy(g => g.SystemId).ToList().Count;
                stats.Strategies.Unique_Autotraded_Forex = atSystems.Where(i => i.ForexPercent > 0).GroupBy(g => g.SystemId).ToList().Count;
                stats.Strategies.Unique_Autotraded_Futures = atSystems.Where(i => i.FuturesPercent > 0).GroupBy(g => g.SystemId).ToList().Count;
                stats.Strategies.Unique_Autotraded_Stocks = atSystems.Where(i => i.StocksPercent > 0).GroupBy(g => g.SystemId).ToList().Count;
                stats.Strategies.Unique_Autotraded_Options = atSystems.Where(i => i.OptionsPercent > 0).GroupBy(g => g.SystemId).ToList().Count;
                stats.Strategies.Total_BrokerTransmit = btSystems.Count;
                logger.Debug("End C2Systems queries");

                #endregion

                #region OpenOrders

                logger.Debug("Start OpenOrders queries");
                List<Database.Entities.Order> openOrders;
                if (this.BrokerID > 0)
                    openOrders = context.Orders.Where(i => (i.OrderStatus == C2Common.Order.QF.OrdStatus.NEW || i.OrderStatus == C2Common.Order.QF.OrdStatus.PENDING_NEW || i.OrderStatus == C2Common.Order.QF.OrdStatus.RISK_SYSTEM || i.OrderStatus == C2Common.Order.QF.OrdStatus.VIRTUAL) && i.BrokerId == brokerId).ToList();
                else
                    openOrders = context.Orders.Where(i => (i.OrderStatus == C2Common.Order.QF.OrdStatus.NEW || i.OrderStatus == C2Common.Order.QF.OrdStatus.PENDING_NEW || i.OrderStatus == C2Common.Order.QF.OrdStatus.RISK_SYSTEM || i.OrderStatus == C2Common.Order.QF.OrdStatus.VIRTUAL) && i.BrokerId != BrokerId.Demo && i.BrokerId != BrokerId.PaperTrade).ToList();

                stats.Orders.Total_Open_Orders = openOrders.Count;
                stats.Orders.Forex_Open_Orders = openOrders.Where(i => i.SecurityType == "FOR").ToList().Count;
                stats.Orders.Futures_Open_Orders = openOrders.Where(i => i.SecurityType == "FUT").ToList().Count;
                stats.Orders.Stocks_Open_Orders = openOrders.Where(i => i.SecurityType == "CS").ToList().Count;
                stats.Orders.Options_Open_Orders = openOrders.Where(i => i.SecurityType == "OPT").ToList().Count;
                stats.Orders.Total_RiskSystem_Orders = openOrders.Where(i => i.OrderStatus == C2Common.Order.QF.OrdStatus.RISK_SYSTEM).ToList().Count;
                logger.Debug("End OpenOrders queries");

                #endregion

                #region All orders

                //logger.Debug("Start All orders queries");
                var allOrdersQuery = $"SELECT transacttime,brokerid FROM orders WHERE transacttime >= '{utc90DaysAgo:yyyy-MM-dd hh:mm:ss}' AND origin <> 9";
                if (this.BrokerID > 0)
                    allOrdersQuery += $" AND BrokerId = {this.BrokerID}";
                else
                    allOrdersQuery += $" AND brokerid <> {(int)BrokerId.Demo} AND brokerid <> {(int)BrokerId.PaperTrade}";
                var all90DaysSqlQuery = context.Database.SqlQuery<O>(allOrdersQuery, new object[] { });
                var allOrdersIn90Days = all90DaysSqlQuery.ToList();
                stats.Orders.Orders_90_Days = allOrdersIn90Days.Count;
                var allOrdersIn60Days = allOrdersIn90Days.Where(i => i.TransactTime >= utc60DaysAgo).ToList();
                stats.Orders.Orders_60_Days = allOrdersIn60Days.Count;
                var allOrdersIn30Days = allOrdersIn60Days.Where(i => i.TransactTime >= utc30DaysAgo).ToList();
                stats.Orders.Orders_30_Days = allOrdersIn30Days.Count;
                stats.Orders.Orders_7_Days = allOrdersIn30Days.Where(i => i.TransactTime >= utc7DaysAgo).ToList().Count;

                #region Old code

                //this is cute code but it runs far roo slowly. LINQ is not good at optimizing queries
                //var openOrdersQuery = @"SELECT COUNT(*) FROM orders WHERE ordertime >= '" + utc90DaysAgo.ToString() + @"' AND orderstatus<>'U' AND brokerid<>31 && brokerid<>30";
                //stats.OrdersIn7Days = context.Orders.Where(i => i.OrderTime >= utc7DaysAgo && i.OrderStatus != "U" && i.BrokerId != BrokerId.Demo && i.BrokerId != BrokerId.PaperTrade).GroupBy(g => g.OrigClOrdId).ToList().Count;
                //stats.OrdersIn30Days = context.Orders.Where(i => i.OrderTime >= utc30DaysAgo && i.OrderStatus != "U" && i.BrokerId != BrokerId.Demo && i.BrokerId != BrokerId.PaperTrade).GroupBy(g => g.OrigClOrdId).ToList().Count;
                //stats.OrdersIn60Days = context.Orders.Where(i => i.OrderTime >= utc60DaysAgo && i.OrderStatus != "U" && i.BrokerId != BrokerId.Demo && i.BrokerId != BrokerId.PaperTrade).GroupBy(g => g.OrigClOrdId).ToList().Count;
                //var ret = context.Orders.Where(i => i.OrderTime >= utc30DaysAgo  && i.OrderStatus != "U" && i.BrokerId != BrokerId.Demo && i.BrokerId != BrokerId.PaperTrade).Select(s => new { s.OrderTime, s.OrigClOrdId, s.OrderStatus, s.BrokerId }).ToList();

                //Takes 2x longer with this code. God LINQ sucks.
                //var allOrdersQuery =
                //from o in context.Orders
                //where o.OrderTime >= utc90DaysAgo && o.OrderStatus != "U" && o.BrokerId != BrokerId.Demo && o.BrokerId != BrokerId.PaperTrade
                //select new
                //{
                //    OrderTime = o.OrderTime,
                //    OrigClOrdId = o.OrigClOrdId,
                //};

                //var allOrders90Days = allOrdersQuery.ToList();
                //stats.OrdersIn90Days = allOrders90Days.Count;
                //stats.OrdersIn60Days = allOrders90Days.Where(i => i.OrderTime >= utc60DaysAgo).ToList().Count;
                //stats.OrdersIn30Days = allOrders90Days.Where(i => i.OrderTime >= utc30DaysAgo).ToList().Count;

                #endregion

                //logger.Debug("End All Orders queries");

                #endregion

                #region FilledOrder

                logger.Debug("Start FilledOrder queries");
                var allFillsQuery = $"SELECT OrigClOrdId, TransactTime FROM executions WHERE transacttime >= '{utc90DaysAgo.ToString("yyyy-MM-dd hh:mm:ss")}' AND orderstatus = '2'";
                if (this.BrokerID > 0)
                    allFillsQuery += $" AND BrokerId = {this.BrokerID}";
                else
                    allFillsQuery += $" AND brokerid <> {(int)BrokerId.Demo} AND brokerid <> {(int)BrokerId.PaperTrade}";


                var all90DaysFillsSqlQuery = context.Database.SqlQuery<O>(allFillsQuery, new object[] { });
                var all90DaysFills = all90DaysFillsSqlQuery.ToList();
                stats.Executions.Filled_Orders_90_Days = all90DaysFills.Count;
                var all60DaysFills = all90DaysFills.Where(i => i.TransactTime > utc60DaysAgo).ToList();
                stats.Executions.Filled_Orders_60_Days = all60DaysFills.Count;
                var all30DaysFills = all60DaysFills.Where(i => i.TransactTime > utc30DaysAgo).ToList();
                stats.Executions.Filled_Orders_30_Days = all30DaysFills.Count;
                stats.Executions.Filled_Orders_7_Days = all30DaysFills.Where(i => i.TransactTime > utc7DaysAgo).ToList().Count;
                logger.Debug("End FilledOrder queries");

                #endregion

                #region Positions

                logger.Debug("Start Positions queries");
                List<Database.Entities.Position> positions;
                if (this.BrokerID > 0)
                    positions = context.Positions.Where(i => i.BrokerId == brokerId).ToList();
                else
                    positions = context.Positions.Where(i => i.BrokerId != BrokerId.Demo && i.BrokerId != BrokerId.PaperTrade).ToList();

                logger.Debug("Positions queries 2");
                stats.Positions.Total_Open_Positions = positions.Count;
                stats.Positions.Forex_Open_Positions = positions.Where(i => i.SecurityType == "FOR").ToList().Count;
                stats.Positions.Futures_Open_Positions = positions.Where(i => i.SecurityType == "FUT").ToList().Count;
                stats.Positions.Stocks_Open_Positions = positions.Where(i => i.SecurityType == "CS").ToList().Count;
                stats.Positions.Options_Open_Positions = positions.Where(i => i.SecurityType == "OPT").ToList().Count;
                logger.Debug("End Positions queries");

                #endregion

                #region Market Data

                logger.Debug("Start MarketData queries");

                IEnumerable<InstrumentUtils.Instrument> uniquePositionInstruments = positions.GroupBy(g => new { g.Symbol, g.MaturityMonthYear, g.PutOrCall, g.StrikePrice }).Select(s => s.FirstOrDefault()).Select( g => new InstrumentUtils.Instrument(g.SecurityType, g.Symbol, g.Currency, g.SecurityExchange, g.MaturityMonthYear, g.UnderlyingMaturityMonthYear, g.PutOrCall, g.StrikePrice, true));
                IEnumerable<InstrumentUtils.Instrument> uniqueOrdersInstruments = openOrders.GroupBy(g => new { g.Symbol, g.MaturityMonthYear, g.PutOrCall, g.StrikePrice }).Select(s => s.FirstOrDefault()).Select(g => new InstrumentUtils.Instrument(g.SecurityType, g.Symbol, g.Currency, g.SecurityExchange, g.MaturityMonthYear, g.UnderlyingMaturityMonthYear, g.PutOrCall, g.StrikePrice, true));
                List<InstrumentUtils.Instrument> uniqueInstruments = new List<InstrumentUtils.Instrument>();

                logger.Debug($"uniquePositionInstruments={uniquePositionInstruments.Count()}");
                logger.Debug($"uniqueOrdersInstruments={uniqueOrdersInstruments.Count()}");

                foreach (InstrumentUtils.Instrument instr in uniquePositionInstruments)
                    uniqueInstruments.Add(instr);

                foreach (InstrumentUtils.Instrument instr in uniqueOrdersInstruments)
                {
                    if (null == uniqueInstruments.FirstOrDefault(i => i.SecurityType == instr.SecurityType
                        && i.C2RootSymbol == instr.C2RootSymbol
                        && i.MaturityMonthYear == instr.MaturityMonthYear
                        && i.PutOrCall == instr.PutOrCall
                        && i.StrikePrice == instr.StrikePrice))
                        uniqueInstruments.Add(instr);
                }

                logger.Debug($"Merged Instruments={uniqueInstruments.Count()}");

                stats.MarketData.Total_MarketData_Subscriptions = uniqueInstruments.Count();
                stats.MarketData.Forex_MarketData_Subscriptions = uniqueInstruments.Where(i => i.SecurityType == "FOR").ToList().Count;
                stats.MarketData.Futures_MarketData_Subscriptions = uniqueInstruments.Where(i => i.SecurityType == "FUT").ToList().Count;
                stats.MarketData.Stocks_MarketData_Subscriptions = uniqueInstruments.Where(i => i.SecurityType == "CS").ToList().Count;
                stats.MarketData.Options_MarketData_Subscriptions = uniqueInstruments.Where(i => i.SecurityType == "OPT").ToList().Count;
                
                logger.Debug("End MarketData queries");

                #endregion
            }

            var response = new GetAutotradeStatsSnapshotResponse
            {
                Stats = stats,
            };

            # region Save to db

            if (savedata)
            {
                using (Database.Statistics.Entities.Context context = new Database.Statistics.Entities.Context())
                {
                    var i = context.Autotrades.Create();
                    i.AccountsAum = stats.Accounts.AUM;
                    i.AccountsAutotrading = stats.Accounts.Autotrading_Accounts;
                    i.AccountsBrokertransmit = stats.Accounts.BrokerTransmit_Accounts;
                    i.FilledOrders30Days = stats.Executions.Filled_Orders_30_Days;
                    i.FilledOrders60Days = stats.Executions.Filled_Orders_60_Days;
                    i.FilledOrders7Days = stats.Executions.Filled_Orders_7_Days;
                    i.FilledOrders90Days = stats.Executions.Filled_Orders_90_Days;
                    i.OrdersTotalOpen = stats.Orders.Total_Open_Orders;
                    i.OrdersTotalOpenForex = stats.Orders.Forex_Open_Orders;
                    i.OrdersTotalOpenFutures = stats.Orders.Futures_Open_Orders;
                    i.OrdersTotalOpenOptions = stats.Orders.Options_Open_Orders;
                    i.OrdersTotalOpenStocks = stats.Orders.Stocks_Open_Orders;
                    i.PositionsTotalOpen = stats.Positions.Total_Open_Positions;
                    i.PositionsTotalOpenForex = stats.Positions.Forex_Open_Positions;
                    i.PositionsTotalOpenFutures = stats.Positions.Futures_Open_Positions;
                    i.PositionsTotalOpenOptions = stats.Positions.Options_Open_Positions;
                    i.PositionsTotalOpenStocks = stats.Positions.Stocks_Open_Positions;
                    i.StrategiesTotalAutotraded = stats.Strategies.Total_Autotraded;
                    i.StrategiesTotalAutotradedForex = stats.Strategies.Total_Autotraded_Forex;
                    i.StrategiesTotalAutotradedFutures = stats.Strategies.Total_Autotraded_Futures;
                    i.StrategiesTotalAutotradedOptions = stats.Strategies.Total_Autotraded_Options;
                    i.StrategiesTotalAutotradedStocks = stats.Strategies.Total_Autotraded_Stocks;
                    i.StrategiesTotalBrokertransmit = stats.Strategies.Total_BrokerTransmit;
                    i.StrategiesUniqueAutotraded = stats.Strategies.Unique_Autotraded;
                    i.StrategiesUniqueAutotradedForex = stats.Strategies.Unique_Autotraded_Forex;
                    i.StrategiesUniqueAutotradedFutures = stats.Strategies.Unique_Autotraded_Futures;
                    i.StrategiesUniqueAutotradedOptions = stats.Strategies.Unique_Autotraded_Options;
                    i.StrategiesUniqueAutotradedStocks = stats.Strategies.Unique_Autotraded_Stocks;
                    i.TotalOrders30Days = stats.Orders.Orders_30_Days;
                    i.TotalOrders60Days = stats.Orders.Orders_60_Days;
                    i.TotalOrders7Days = stats.Orders.Orders_7_Days;
                    i.TotalOrders90Days = stats.Orders.Orders_90_Days;
                    i.Utcdate = stats.UTCDate;
                    context.Autotrades.Add(i);
                    context.SaveChanges();
                }
            }

            #endregion

            return response;
        }
    }

    class O
    {
        public BrokerId BrokerId { get; set; }

        public DateTime TransactTime { get; set; }
    }

    class S
    {
        public int UserId { get; set; }
        public string AccountName { get; set; }
        public BrokerId BrokerId { get; set; }
        public int SystemId { get; set; }
        public double ForexPercent { get; set; }
        public double FuturesPercent { get; set; }
        public double StocksPercent { get; set; }
        public double OptionsPercent { get; set; }
        public bool IsPublisher { get; set; }

        public double NetLiquidationValue { get; set; }
    }

    [ApiResponse]
    public class GetAutotradeStatsSnapshotResponse: BaseResponse
    {
        [ApiMember(Description = @"Autotrade stats", IsRequired = true)]
        public AutotradeStatsDTO Stats { get; set; } = new AutotradeStatsDTO();
    }
}
