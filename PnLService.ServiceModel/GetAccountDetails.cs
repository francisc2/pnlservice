﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Data.Entity;
using ServiceStack;
using C2Enums;

namespace PnLService.ServiceModel
{
    [Api]
    [Route("/getaccountdetails", "GET", Summary = @"Returns the account details for all BrokerCodes")]
    [Route("/getaccountdetails/{BrokerCode}", "GET", Summary = @"Returns all the account details for the specified BrokerCode")]
    [Route("/getaccountdetails/{Account}", "GET", Summary = @"Returns all the account details for the specified Account. BrokerCode is ignored")]
	public class GetAccountDetails: IReturn<GetAccountDetailsResponse>
    {
        private static NLog.Logger logger = NLog.LogManager.GetLogger("GetAccountDetails");

        public GetAccountDetails()
        {
        }

        [ApiMember(Description = @"Account", IsRequired = false)]
        public string Account { get; set; }


        [ApiMember(Description = @"BrokerCode", IsRequired = false)]
        public int BrokerCode { get; set; }


        public object Get(CancellationToken token, int brokerCode)
        {
            logger.Debug($"{this.GetType().Name}.Get: {this.ToSafeJson()}");
            var response = new GetAccountDetailsResponse();
            using (Database.BrokerAccountInfo.Entities.Context context = new Database.BrokerAccountInfo.Entities.Context())
            {
				#region Get list of accounts

				List<Database.BrokerAccountInfo.Entities.Account> accounts = new List<Database.BrokerAccountInfo.Entities.Account>();
                if (String.IsNullOrEmpty(Account))
                {

                    if (BrokerCode == 0)
                    {
                        //Return all accounts
                        accounts = context.Accounts.ToList();
                    }
                    else
                    {
                        accounts = context.Accounts.Where(i => i.BrokerCode == brokerCode).ToList();
                    }
                }
                else
                {
                    //Return the account that matches
                    //Comparison is case-insensitive. See https://blog.devart.com/mysql-case-sensitive-search-in-entity-framework.html
                    accounts = context.Accounts.Where(i => i.AccountId == Account).ToList();
                }

                #endregion

                foreach (var account in accounts)
                {
                    response.Accounts.Add(new AccountDetailsDTO
                    {
                        Account = account.AccountId,
                        AccountType = account.AccountType,
                        MasterAccount = account.MasterAccountId,
                        BaseCurrency = account.BaseCurrency,
                        BrokerCode = account.BrokerCode.Value,
                        Country = account.Country,
                        CustomerType = account.CustomerType,
                        DateClosed = account.DateClosed,
                        DateFunded = account.DateFunded,
                        DateOpened = account.DateOpened,
                        Email = account.Email,
                        MarginType = account.Capabilities,
                        State = account.State,
                        Street = account.Street,
                        Street2 = account.Street2,
                        Title = account.AccountTitle,
                        Zip = account.Zip,
                    });
                }
            }

            return response;
        }
    }

    [ApiResponse]
    public class GetAccountDetailsResponse: BaseResponse
    {
        [ApiMember(Description = @"Account details", IsRequired = true)]
        public List<AccountDetailsDTO> Accounts { get; set; } = new List<AccountDetailsDTO>();
    }
}
