﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2Enums;

namespace PnLService.ServiceModel
{
    public class ActiveAccountStatsDTO
    {
        public int Id { get; set; }
        public string Account { get; set; }
        public int PersonId { get; set; }
        public long BrokerId { get; set; }
        public int StrategyId { get; set; }
        public int IB { get; set; }
        public int SalesRep { get; set; }
        public int? WhiteLabelSiteId { get; set; }
        public string GeoSiteId { get; set; }
        public double? NetLiqu { get; set; }
        public DateTime? LastBrokerNetLiquUpdateUtc { get; set; }
        public bool StocksEnabled { get; set; }
        public bool FuturesEnabled { get; set; }
        public bool OptionsEnabled { get; set; }
        public bool ForexEnabled { get; set; }
        public bool CryptoEnabled { get; set; }
        public bool IsSyncEnabled { get; set; }
        public bool IsManualConfirm { get; set; }
        public bool IsDisabled { get; set; }
        public bool IsBrokerTransmit{ get; set; }
    }
}
