﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack;

namespace PnLService.ServiceModel
{
	[ApiResponse(Description = "Autotrade Stats. All brokers except Demo and Papertrade will be included unless specified")]
	public class AutotradeStatsDTO
	{
		[ApiMember(Description = @"UTC DateTime of report")]
		public DateTime UTCDate  { get; set; }


		[ApiMember(Description = @"Account Statistics")]
		public AccountStats Accounts  { get; set; } = new AccountStats();


		[ApiMember(Description = @"Order Statistics")]
		public OrderStats Orders  { get; set; } = new OrderStats();


		[ApiMember(Description = @"Execution Statistics")]
		public ExecutionStats Executions  { get; set; } = new ExecutionStats();


		[ApiMember(Description = @"Position Statistics")]
		public PositionStats Positions  { get; set; } = new PositionStats();


		[ApiMember(Description = @"Strategy Statistics")]
		public StrategyStats Strategies  { get; set; } = new StrategyStats();


		[ApiMember(Description = @"MarketData Statistics")]
		public MarketDataStats MarketData { get; set; } = new MarketDataStats();

}
}
