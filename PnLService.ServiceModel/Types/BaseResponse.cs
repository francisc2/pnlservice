﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceStack;

namespace PnLService.ServiceModel
{
	[ApiResponse]
    public class BaseResponse: IHasResponseStatus
    {
		static string OK = System.Net.HttpStatusCode.OK.ToString();


		/// <summary>
		/// Default Status is OK
		/// </summary>
		public BaseResponse()
        {
            ResponseStatus = new ResponseStatus(OK, string.Empty);
        }

		public BaseResponse(System.Net.HttpStatusCode statusCode, string errorDescription)
        {
            ResponseStatus = new ResponseStatus(statusCode.ToString(), errorDescription);
        }


		[ApiMember(Description = @"Status of request",  IsRequired = true)]
		public ResponseStatus ResponseStatus { get; set; }
    }

    public class ResponseBase<T> : IHasResponseStatus
    {
        [ApiMember(Description = "Result array")]
        public virtual List<T> Results { get; set; }


        [ApiMember(Description = "The ResponseStatus. The Statuscode will be the same as in the HTTP header")]
        public virtual ResponseStatus ResponseStatus { get; set; } = new ResponseStatus("200");
    }
}
