﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack;

namespace PnLService.ServiceModel
{
	public class OrderStats
	{
		[ApiMember(Description = @"The total number of currently open orders")]
		public int Total_Open_Orders  { get; set; }

		[ApiMember(Description = @"The total number of currently open orders waiting in the Risk System")]
		public int Total_RiskSystem_Orders  { get; set; }


		[ApiMember(Description = @"The total number of currently open Forex orders")]
		public int Forex_Open_Orders  { get; set; }

		[ApiMember(Description = @"The total number of currently open Futures orders")]
		public int Futures_Open_Orders  { get; set; }

		[ApiMember(Description = @"The total number of currently open Stocks orders")]
		public int Stocks_Open_Orders  { get; set; }

		[ApiMember(Description = @"The total number of currently open Options orders")]
		public int Options_Open_Orders  { get; set; }
		


		[ApiMember(Description = @"The total number of orders in the last 7 days")]
		public int Orders_7_Days { get; set; }

		[ApiMember(Description = @"The total number of orders in the last 30 days")]
		public int Orders_30_Days { get; set; }

		[ApiMember(Description = @"The total number of orders in the last 60 days")]
		public int Orders_60_Days { get; set; }

		[ApiMember(Description = @"The total number of orders in the last 90 days")]
		public int Orders_90_Days { get; set; }
	}
}
