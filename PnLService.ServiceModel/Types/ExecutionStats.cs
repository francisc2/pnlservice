﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack;

namespace PnLService.ServiceModel
{
	public class ExecutionStats
	{
		[ApiMember(Description = @"The total number of executed orders in the last 7 days")]
		public int Filled_Orders_7_Days { get; set; }

		[ApiMember(Description = @"The total number of executed orders in the last 30 days")]
		public int Filled_Orders_30_Days { get; set; }

		[ApiMember(Description = @"The total number of executed orders in the last 60 days")]
		public int Filled_Orders_60_Days { get; set; }

		[ApiMember(Description = @"The total number of executed orders in the last 90 days")]
		public int Filled_Orders_90_Days { get; set; }
	}
}
