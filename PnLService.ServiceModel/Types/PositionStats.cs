﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack;

namespace PnLService.ServiceModel
{
	public class PositionStats
	{

		[ApiMember(Description = @"The total number of currently open positions. Includes BT positions")]
		public int Total_Open_Positions  { get; set; }


		[ApiMember(Description = @"The total number of currently open Forex positions")]
		public int Forex_Open_Positions  { get; set; }

		[ApiMember(Description = @"The total number of currently open Futures positions")]
		public int Futures_Open_Positions  { get; set; }

		[ApiMember(Description = @"The total number of currently open Stocks positions")]
		public int Stocks_Open_Positions  { get; set; }

		[ApiMember(Description = @"The total number of currently open Options positions")]
		public int Options_Open_Positions  { get; set; }
	}
}
