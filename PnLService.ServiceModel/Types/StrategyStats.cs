﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack;

namespace PnLService.ServiceModel
{
	public class StrategyStats
	{
		[ApiMember(Description = @"The total current number of Autotraded Strategies")]
		public int Total_Autotraded  { get; set; }

		[ApiMember(Description = @"The total current number of BrokerTransmit Strategies")]
		public int Total_BrokerTransmit  { get; set; }



		[ApiMember(Description = @"The total number of Autotraded Forex Strategies. Autotraders may have more than one asset class enabled per system.")]
		public int Total_Autotraded_Forex  { get; set; }

		[ApiMember(Description = @"The total number of Autotraded Stocks Strategies. Autotraders may have more than one asset class enabled per system.")]
		public int Total_Autotraded_Stocks  { get; set; }

		[ApiMember(Description = @"The total number of Autotraded Futures Strategies. Autotraders may have more than one asset class enabled per system.")]
		public int Total_Autotraded_Futures  { get; set; }

		[ApiMember(Description = @"The total number of Autotraded Options Strategies. Autotraders may have more than one asset class enabled per system.")]
		public int Total_Autotraded_Options  { get; set; }



		[ApiMember(Description = @"The current number of unique Autotraded Strategies")]
		public int Unique_Autotraded  { get; set; }

		[ApiMember(Description = @"The current number of unique Autotraded Forex Strategies. Autotraders may have more than one asset class enabled per system.")]
		public int Unique_Autotraded_Forex  { get; set; }

		[ApiMember(Description = @"The current number of unique Autotraded Stocks Strategies. Autotraders may have more than one asset class enabled per system.")]
		public int Unique_Autotraded_Stocks  { get; set; }

		[ApiMember(Description = @"The current number of unique Autotraded Futures Strategies. Autotraders may have more than one asset class enabled per system.")]
		public int Unique_Autotraded_Futures  { get; set; }

		[ApiMember(Description = @"The current number of unique Autotraded Options Strategies. Autotraders may have more than one asset class enabled per system.")]
		public int Unique_Autotraded_Options  { get; set; }
	}
}