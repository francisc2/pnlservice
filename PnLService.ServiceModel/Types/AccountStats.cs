﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack;


namespace PnLService.ServiceModel
{
	public class AccountStats
	{
		[ApiMember(Description = @"The current number of unique Autotrading brokerage accounts")]
		public int Autotrading_Accounts { get; set; }

		
		[ApiMember(Description = @"The current number of unique BrokerTransmit brokerage accounts. Can overlap with Autotrading_Accounts")]
		public int BrokerTransmit_Accounts { get; set; }
		

		[ApiMember(Description = @"The total assets under management of Autotrading brokerage accounts. Includes BT accounts")]
		public int AUM { get; set; }

		//[ApiMember(Description = @"The current number of unique Autotrading C2 personIDs")]
		//public int CurrentAutotradingPersonIds { get; set; }
	}
}
