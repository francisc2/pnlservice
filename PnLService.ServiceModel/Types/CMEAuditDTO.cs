﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack;

namespace PnLService.ServiceModel
{
    public class CMEAuditDTO
    {
        [ApiMember(Description = @"M/d/yyyy h:mm:ss tt in US/Central time", IsRequired = true, Name = "Timestamp US/Central")]
        public string Timestamp  { get; set; }

        
        [ApiMember(Description = @"The Broker (not CME) Account", IsRequired = true)]
		public string Account { get; set; }


        [ApiMember(Description = @"Messsge Type", IsRequired = false)]
        public string MsgType { get; set; }


        [ApiMember(Description = @"The Broker (Not CME) OrderId", IsRequired = false)]
        public string OrderId { get; set; }


        [ApiMember(Description = @"The C2 SignalId", IsRequired = false)]
        public int SignalId { get; set; }


        [ApiMember(Description = @"The C2 SystemId", IsRequired = false)]
        public int SystemId { get; set; }


        [ApiMember(IsRequired = false)]
        public string Message { get; set; }
    }
}
