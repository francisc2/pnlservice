﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack;

namespace PnLService.ServiceModel
{
    public class MarketDataStats
    {
        [ApiMember(Description = @"The total number of unique Market Data subscriptions")]
        public int Total_MarketData_Subscriptions { get; set; }

        [ApiMember(Description = @"The total number of unique Stocks subscriptions")]
        public int Stocks_MarketData_Subscriptions { get; set; }

        [ApiMember(Description = @"The total number of unique Futures subscriptions")]
        public int Futures_MarketData_Subscriptions { get; set; }

        [ApiMember(Description = @"The total number of unique Options subscriptions")]
        public int Options_MarketData_Subscriptions { get; set; }

        [ApiMember(Description = @"The total number of unique Forex subscriptions")]
        public int Forex_MarketData_Subscriptions { get; set; }

    }
}
