﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack;

namespace PnLService.ServiceModel
{
	[ApiResponse(Description = "Account details. Data is only available for linked IB accounts")]
	public class AccountDetailsDTO
	{
		[ApiMember(Description = @"Account")]
		public string Account  { get; set; } = string.Empty;

		[ApiMember(Description = @"Base Account Currency")]
		public string BaseCurrency  { get; set; } = string.Empty;

		[ApiMember(Description = @"Account Title")]
		public string Title  { get; set; } = string.Empty;

		[ApiMember(Description = @"Street")]
		public string Street  { get; set; } = string.Empty;

		[ApiMember(Description = @"Street2")]
		public string Street2  { get; set; } = string.Empty;

		[ApiMember(Description = @"State")]
		public string State  { get; set; } = string.Empty;

		[ApiMember(Description = @"Zip")]
		public string Zip  { get; set; } = string.Empty;

		[ApiMember(Description = @"Country")]
		public string Country  { get; set; } = string.Empty;

		[ApiMember(Description = @"Email")]
		public string Email  { get; set; } = string.Empty;

		[ApiMember(Description = @"Account Type")]
		public string AccountType  { get; set; } = string.Empty;

		[ApiMember(Description = @"Customer Type")]
		public string CustomerType  { get; set; } = string.Empty;

		[ApiMember(Description = @"Master Account")]
		public string MasterAccount  { get; set; } = string.Empty;

		[ApiMember(Description = @"Date Opened UTC")]
		public DateTime? DateOpened  { get; set; } = null;

		[ApiMember(Description = @"Date Closed UTC")]
		public DateTime? DateClosed  { get; set; } = null;

		[ApiMember(Description = @"Date Funded UTC")]
		public DateTime? DateFunded  { get; set; } = null;

		[ApiMember(Description = @"Margin Type")]
		public string MarginType  { get; set; } = string.Empty;

		[ApiMember(Description = @"Broker Code")]
		public int BrokerCode  { get; set; }
	}
}
