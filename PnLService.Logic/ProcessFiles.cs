﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using PgpCore;
using Database.BrokerAccountInfo.Entities;
using System.Text.RegularExpressions;

namespace PnLService.Logic
{
    /// <summary>
    /// b) parse them and store values in the database
    /// c) mark processed files so they are not processed again.
    /// d) keep all files locally and purge them once in a while as the SSD gets full.
    /// e) files are downloaded in RAW folder, then moved and decrypted into the Decrypted folder.
    /// f) ignored files (those we don't process) are moved as-is from RAW to the UnprocessedArchive folder.
    /// This endpoint will be called after DownloadFiles.
    /// We can also call it at any time manually though a web request.
    /// See https://www.ibkrguides.com/reportingintegration/topics/
    /// </summary>
    public class ProcessFiles
    {
        static readonly NLog.Logger logger = NLog.LogManager.GetLogger("ProcessFiles");
        CancellationToken token = CancellationToken.None;
        DirectoryInfo rawDirectoryInfo, activityRawDirectoryInfo, archiveDirectoryInfo, keyDirectoryInfo, decryptedDirectoryInfo;
        BrokerCode brokerCode;

        public ProcessFiles(CancellationToken token, BrokerCode brokerCode)
        {
            this.token = token;
            this.brokerCode = brokerCode;

            //Make sure local paths exist
            keyDirectoryInfo = Directory.CreateDirectory($"{Properties.Settings.Default.LocalFolder}\\Keys");
            rawDirectoryInfo = Directory.CreateDirectory($"{Properties.Settings.Default.LocalFolder}\\{brokerCode.Name}\\Raw");
            archiveDirectoryInfo = Directory.CreateDirectory($"{Properties.Settings.Default.LocalFolder}\\{brokerCode.Name}\\UnprocessedArchive");
            decryptedDirectoryInfo = Directory.CreateDirectory($"{Properties.Settings.Default.LocalFolder}\\{brokerCode.Name}\\Decrypted");
            activityRawDirectoryInfo = Directory.CreateDirectory($"{Properties.Settings.Default.LocalFolder}\\{brokerCode.Name}\\ActivityRaw");
        }

        public void Run()
        {
            long getallFiles = 0;
            long decryptFiles = 0;
            long castRowsToObjects = 0;
            long processFiles = 0;
            long saveToDb = 0;
            long moveFiles = 0;
            long deleteFiles = 0;
            Stopwatch sw = Stopwatch.StartNew();

            #region Get a list of files

            var allRawFiles = rawDirectoryInfo.EnumerateFiles("*.csv.pgp");
            List<string> filesToDelete = new List<string>();

            int numFiles = allRawFiles.Count();
            if (numFiles == 0)
            {
                logger.Info($"No {brokerCode.Name} files to process");
                return;
            }
            else
            {
                logger.Info($"Processing {numFiles} {brokerCode.Name} files");
            }

            getallFiles = sw.ElapsedMilliseconds;
            sw.Reset();

            #endregion

            using (FileStream privateKeyStream = new FileStream($@"{keyDirectoryInfo.FullName}\\{brokerCode.KeyFilename}", FileMode.Open))
            {
                var decryptionKey = new EncryptionKeys(privateKeyStream.ReadToEnd(), brokerCode.KeyPassphrase);
                var readerConf = new CsvHelper.Configuration.CsvConfiguration(CultureInfo.InvariantCulture)
                {
                    Delimiter = ",",
                    HasHeaderRecord = true,
                };


                //Loop through files.
                //If any record in a file cannot be processed due to error
                //, the file is kept in the RAW folder and the pending db changes are discarded.
                foreach (var file in allRawFiles)
                {
                    using (var context = new Context())
                    {
                        ActivityRecord currentRow = null;
                        try
                        {
                            if (token.IsCancellationRequested)
                                break;

                            logger.Info($"Processing RAW file: {file.Name}");

                            #region Skip if not Activity, Account, CashReport and NAV

                            FileType fileType = Utils.GetFileType(file.Name);
                            if (fileType != FileType.Activity
                                && fileType != FileType.Account
                                && fileType != FileType.NAV
                                && fileType != FileType.CashReport)
                            {
                                logger.Warn($"{file.Name} is not supported but it is in the RAW folder. Moving it to the UnprocessedArchive folder for safekeeping");
                                file.MoveTo($"{archiveDirectoryInfo.FullName}\\{file.Name}");
                                continue;
                            }

                            #endregion

                            #region Decrypt the file

                            //See https://github.com/mattosaurus/PgpCore
                            sw.Start();
                            string decryptedString;
                            using (PGP pgp = new PGP(decryptionKey))
                            {
                                using (FileStream inputFileStream = file.OpenRead())
                                using (MemoryStream outputStream = new MemoryStream())
                                {
                                    if (file.Length == 0)
                                    {
                                        File.Delete(file.FullName);
                                        continue;
                                    }
                                    else
                                    {
                                        pgp.DecryptStream(inputFileStream, outputStream);
                                        decryptedString = outputStream.ReadToEnd();
                                    }
                                }
                            }

                            decryptFiles += sw.ElapsedMilliseconds;
                            sw.Reset();

                            #endregion

                            #region Process File

                            #region Process Header (deprecated--they are not sending it)

                            //if (false == csv.Read())
                            //{
                            //    logger.Debug($"Skipping empty file: {file.Name}.");
                            //    continue;
                            //}

                            //if (false == csv.ReadHeader())
                            //{
                            //    logger.Debug($"Missing header. Skipping file: {file.Name}.");
                            //    continue;
                            //}

                            //if (csv.HeaderRecord == null || csv.HeaderRecord.Length < 7)
                            //{
                            //    logger.Info($"Can't read the file header: {string.Join("|", csv.HeaderRecord)}. Skipping {file.Name}.");
                            //    continue;
                            //}

                            //if (false == decimal.TryParse((string)csv.HeaderRecord.GetValue(5), out decimal version) 
                            //    || version <= 0)
                            //{
                            //    logger.Error($"Error reading the file version: HeaderRecord={string.Join("|", csv.HeaderRecord)}");
                            //    continue;
                            //}
                            //logger.Debug($"Version {version}");

                            #endregion

                            if (fileType == FileType.Account)
                            {
                                #region Account

                                //Type,AccountID,AccountTitle,Street,Street2,City,State,Zip,Country,AccountType,CustomerType,BaseCurrency,MasterAccountID,Van,Capabilities,Alias,PrimaryEmail,DateOpened,DateClosed,DateFunded,AccountRepresentative,
                                //D,U5426118,Klara Zentai,Fodor Utca 50/A,1 em 5 a,Budapest 12,HU-BU,1124,Hungary,Individual,Individual,EUR,,,Margin,,zentaiklara1@gmail.com,2021-02-11,,2021-02-10,,
                                using (var reader = new StreamReader(new MemoryStream(decryptedString.ToAsciiBytes())))
                                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                                {
                                    sw.Start();
                                    var rows = csv.GetRecords<AccountRecord>();
                                    castRowsToObjects += sw.ElapsedMilliseconds;
                                    sw.Reset();

                                    foreach (var row in rows)
                                    {
                                        sw.Start();
                                        bool isNewRow = false;

                                        //Only add if new
                                        var entity = context.Accounts.Where(i => i.AccountId == row.AccountID).FirstOrDefault();
                                        if (entity == null)
                                        {
                                            isNewRow = true;
                                            entity = context.Accounts.Create();
                                        }

                                        entity.AccountId = row.AccountID;
                                        entity.AccountTitle = row.AccountTitle;
                                        entity.AccountType = row.AccountType;
                                        entity.BaseCurrency = row.BaseCurrency;
                                        entity.BrokerCode = GetBrokerCodeFromFileName(file.Name, brokerCode.Code);
                                        entity.Capabilities = row.Capabilities;
                                        entity.City = row.City;
                                        entity.Country = row.Country;
                                        entity.CustomerType = row.CustomerType;
                                        entity.Email = row.PrimaryEmail;
                                        entity.MasterAccountId = row.MasterAccountID;
                                        entity.State = row.State;
                                        entity.Street = row.Street;
                                        entity.Street2 = row.Street2;
                                        entity.Zip = row.Zip;

                                        if (DateTime.TryParseExact(row.DateOpened, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime DateOpened))
                                            if (entity.DateOpened != DateOpened) entity.DateOpened = DateOpened;

                                        if (DateTime.TryParseExact(row.DateClosed, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime DateClosed))
                                            if (entity.DateClosed != DateClosed) entity.DateClosed = DateClosed;

                                        if (DateTime.TryParseExact(row.DateFunded, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime DateFunded))
                                            if (entity.DateFunded != DateFunded) entity.DateFunded = DateFunded;


                                        processFiles += sw.ElapsedMilliseconds;
                                        sw.Reset();

                                        if (isNewRow)
                                        {
                                            sw.Start();
                                            context.Accounts.Add(entity);
                                            saveToDb += sw.ElapsedMilliseconds;
                                            sw.Reset();
                                        }
                                    }
                                }

                                #region Old Manual code
                                //using (var reader = new StreamReader(new MemoryStream(decryptedString.ToAsciiBytes())))
                                //                     using (var csv = new CsvReader(reader, config))
                                //                     {
                                //                         while (csv.Read())
                                //                         {
                                //                             var item = context.Accounts.Create();
                                //                             item.AccountId = csv.GetField(1);

                                //                             //Only add if new
                                //                             var existing = context.Accounts.Where(i => i.AccountId == item.AccountId).FirstOrDefault();
                                //                             if (existing != null)
                                //                                 item = existing;
                                //                             else
                                //                                 context.Accounts.Add(item);

                                //                             if (item.AccountType != csv.GetField(0)) item.AccountType = csv.GetField(0);
                                //                             if (item.AccountTitle != csv.GetField(2)) item.AccountTitle = csv.GetField(2);
                                //                             if (item.Street != csv.GetField(3)) item.Street = csv.GetField(3);
                                //                             if (item.Street2 != csv.GetField(4)) item.Street2 = csv.GetField(4);
                                //                             if (item.City != csv.GetField(5)) item.City = csv.GetField(5);
                                //                             if (item.State != csv.GetField(6)) item.State = csv.GetField(6);
                                //                             if (item.Zip != csv.GetField(7)) item.Zip = csv.GetField(7);
                                //                             if (item.Country != csv.GetField(8)) item.Country = csv.GetField(8);
                                //                             if (item.CustomerType != csv.GetField(10)) item.CustomerType = csv.GetField(10);
                                //                             if (item.BaseCurrency != csv.GetField(11)) item.BaseCurrency = csv.GetField(11);
                                //                             if (item.MasterAccountId != csv.GetField(12)) item.MasterAccountId = csv.GetField(12);
                                //                             if (item.Capabilities != csv.GetField(14)) item.Capabilities = csv.GetField(14);
                                //                             if (item.Email != csv.GetField(16)) item.Email = csv.GetField(16);
                                //                             if (DateTime.TryParseExact(csv.GetField(17), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime DateOpened))
                                //                                 if (item.DateOpened != DateOpened) item.DateOpened = DateOpened;

                                //                             if (DateTime.TryParseExact(csv.GetField(18), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime DateClosed))
                                //                                 if (item.DateClosed != DateClosed) item.DateClosed = DateClosed;

                                //                             if (DateTime.TryParseExact(csv.GetField(19), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime DateFunded))
                                //                                 if (item.DateFunded != DateFunded) item.DateFunded = DateFunded;

                                //                             item.BrokerCode = GetBrokerCodeFromFileName(file.Name, brokerCode.Code);
                                //                             context.SaveChanges();
                                //                         }
                                //}
                                #endregion

                                #endregion
                            }
                            else if (fileType == FileType.NAV)
                            {
                                #region NAV

                                //Type	AccountID	BaseCurrency	Cash	CashCollateral	Stocks	SecuritiesBorrowed	SecuritiesLent	Options	Bonds	Commodities	Funds	Notes	Accruals	DividendAccruals	SoftDollars	Totals	TWR	CFDUnrealizedPL	ForexCFDUnrealizedPL	IPOSubscription
                                //D	U000001	USD	3290021.047	1194746	131578.29	0	-1194746	-206046.7	0	0	0	0	-3665.444604	1200	0	3213087.192	0.714319828	0	0	0
                                //D,U481497,USD,99934.79163819,0,0,0,0,0,0,0,0,0,-602.65569626,0,0,99332.13594193,-2.703418192,0,0,0,

                                using (var reader = new StreamReader(new MemoryStream(decryptedString.ToAsciiBytes())))
                                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                                {
                                    sw.Start();
                                    var rows = csv.GetRecords<NAVRecord>();
                                    castRowsToObjects += sw.ElapsedMilliseconds;
                                    sw.Reset();

                                    foreach (var row in rows)
                                    {
                                        sw.Start();
                                        var entity = context.Navs.Create();

                                        entity.AccountId = row.AccountID;
                                        entity.BaseCurrency = row.BaseCurrency;
                                        entity.Cash = row.Cash;
                                        entity.CashCollateral = row.CashCollateral;
                                        entity.Stocks = row.Stocks;
                                        entity.SecuritiesBorrowed = row.SecuritiesBorrowed;
                                        entity.SecuritiesLent = row.SecuritiesLent;
                                        entity.Options = row.Options;
                                        entity.Bonds = row.Bonds;
                                        entity.Commodities = row.Commodities;
                                        entity.Funds = row.Funds;
                                        entity.Notes = row.Notes;
                                        entity.Accruals = row.Accruals;
                                        entity.DividendAccruals = row.DividendAccruals;
                                        entity.SoftDollars = row.SoftDollars;
                                        entity.Totals = row.Totals;
                                        entity.Twr = row.TWR;
                                        entity.CfdUnrealizedPL = row.CFDUnrealizedPL;
                                        //entity.ForexUnrealizedPL = row.ForexUnrealizedPL;
                                        //entity.IpoSubscription = row.IPOSubscription;
                                        //entity.InsuredBankDepositSweepProgram = row.InsuredBankDepositSweepProgram;
                                        //entity.InsuredBankDepositSweepProgramIntAccrurals = row.InsuredBankDepositSweepProgramIntAccruals;
                                        entity.Date = GetDateFromFileName(Path.GetFileNameWithoutExtension(file.Name));

                                        processFiles += sw.ElapsedMilliseconds;
                                        sw.Reset();

                                        sw.Start();
                                        context.Navs.Add(entity);
                                        saveToDb += sw.ElapsedMilliseconds;
                                        sw.Reset();
                                    }

                                    #region Old manual method
                                    //while (csv.Read())
                                    //{
                                    //    //Headers
                                    //    if (++rowNumber == 1)
                                    //    {
                                    //        continue;
                                    //    }

                                    //    var item = context.Navs.Create();
                                    //    item.AccountId = csv.GetField(1);
                                    //    item.BaseCurrency = csv.GetField(2);
                                    //    item.Cash = ParseDecimal(csv.GetField(3));
                                    //    item.CashCollateral = ParseDecimal(csv.GetField(4));
                                    //    item.Stocks = ParseDecimal(csv.GetField(5));
                                    //    item.SecuritiesBorrowed = ParseDecimal(csv.GetField(6));
                                    //    item.SecuritiesLent = ParseDecimal(csv.GetField(7));
                                    //    item.Options = ParseDecimal(csv.GetField(8));
                                    //    item.Bonds = ParseDecimal(csv.GetField(9));
                                    //    item.Commodities = ParseDecimal(csv.GetField(10));
                                    //    item.Funds = ParseDecimal(csv.GetField(11));
                                    //    item.Notes = ParseDecimal(csv.GetField(12));
                                    //    item.Accruals = ParseDecimal(csv.GetField(13));
                                    //    item.DividendAccruals = ParseDecimal(csv.GetField(14));
                                    //    item.SoftDollars = ParseDecimal(csv.GetField(15));
                                    //    item.Totals = ParseDecimal(csv.GetField(18));
                                    //    item.Twr = ParseDecimal(csv.GetField(19));
                                    //    item.ForexUnrealizedPL = ParseDecimal(csv.GetField(20));
                                    //    item.Date = GetDateFromFileName(Path.GetFileNameWithoutExtension(file.Name));

                                    //    context.Navs.Add(item);
                                    //    context.SaveChanges();
                                    //}

                                    #endregion
                                }

                                #endregion
                            }
                            else if (fileType == FileType.CashReport)
                            {
                                #region CashReport

                                /*
                                 * Type,AccountID,ReportDate,Currency,BaseSummary,Label,Total,Securities,Futures,IBUKL,
                                    D,U7502771,2021-04-19,USD,N,Ending Settled Cash,149044.92069074,149044.92069074,0,0,
                                    D,U7502771,2021-04-19,USD,Y,Commissions,-18.77,-18.77,0,0,
                                */

                                using (var reader = new StreamReader(new MemoryStream(decryptedString.ToAsciiBytes())))
                                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                                {
                                    sw.Start();
                                    var rows = csv.GetRecords<CashReportRecord>();
                                    castRowsToObjects += sw.ElapsedMilliseconds;
                                    sw.Reset();

                                    foreach (var row in rows)
                                    {
                                        sw.Start();

                                        //Only process base currency
                                        if (row.BaseSummary != "Y")
                                            continue;

                                        //Only add if new
                                        bool isNewRow = false;
                                        var entity = context.Cashreports.Where(i => i.Date == row.ReportDate && i.AccountId == row.AccountID && i.BaseCurrency == row.Currency).FirstOrDefault();
                                        if (entity == null)
                                        {
                                            isNewRow = true;
                                            entity = context.Cashreports.Create();
                                        }

                                        entity.Date = row.ReportDate;
                                        entity.AccountId = row.AccountID;
                                        entity.BaseCurrency = row.Currency;
                                        //entity.Futures = row.Futures;
                                        //entity.IBUKL = row.IBUKL;
                                        //entity.Securities = row.Securities;
                                        //entity.Total = row.Total;

                                        if (row.Label.EqualsIgnoreCase("Starting Cash"))
                                            entity.StartingCash = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Commissions"))
                                            entity.Commissions = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Internal Transfers"))
                                            entity.InternalTransfers = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Account Transfers"))
                                            entity.AccountTransfers = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Cash Settling MTM"))
                                            entity.CashSettlingMTM = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Net Trades (Sales)"))
                                            entity.NetTradesSales = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Net Trades (Purchase)"))
                                            entity.NetTradesPurchases = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Cash FX Translation Gain/Loss"))
                                            entity.CashFxGainLoss = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Ending Cash"))
                                            entity.EndingCash = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Ending Settled Cash"))
                                            entity.EndingSettledCash = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Deposits"))
                                            entity.Deposits = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Withdrawals"))
                                            entity.Withdrawals = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Debit Card Activity"))
                                            entity.DebitCardActivity = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Broker Interest Paid and Received"))
                                            entity.BrokerInterest = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Transaction Fees"))
                                            entity.TransactionFees = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Other Fees"))
                                            entity.OtherFees = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Change in Insured Bank Deposits"))
                                            entity.ChangeInInsuredBankDeposits = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Dividends"))
                                            entity.Dividends = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Payment In Lieu of Dividends"))
                                            entity.PaymentInLieuOfDividends = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Advisor Fees"))
                                            entity.AdvisorFees = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("Client Fees"))
                                            entity.ClientFees = row.Total;
                                        else if (row.Label.EqualsIgnoreCase("GST/HST/PST")
                                            || row.Label.EqualsIgnoreCase("Withholding Tax")
                                            || row.Label.EqualsIgnoreCase("Insured Deposit Interest")
                                            || row.Label.EqualsIgnoreCase("Commission Credits Redemption")
                                            || row.Label.EqualsIgnoreCase("CFD Charges")
                                            || row.Label.EqualsIgnoreCase("CFD Realized P/L")
                                            || row.Label.EqualsIgnoreCase("Broker Fees Paid and Received")
                                            || row.Label.EqualsIgnoreCase("Bond Interest Paid and Received")
                                            || row.Label.EqualsIgnoreCase("Sales Tax")
                                            || row.Label.EqualsIgnoreCase("Forex CFD Realized P/L")
                                            || row.Label.EqualsIgnoreCase("Debit/Prepaid Card Activity")
                                            || row.Label.EqualsIgnoreCase("Bill Pay")
                                            || row.Label.EqualsIgnoreCase("IPO Subscription")
                                            || row.Label.EqualsIgnoreCase("CYEP/Broker Fees Paid and Received")
                                            || row.Label.EqualsIgnoreCase("871(m) Withholding"))
                                            continue;
                                        else
                                            throw new Exception($"Unknown field '{row.Label}' in {file.Name} | File={decryptedString}");

                                        processFiles += sw.ElapsedMilliseconds;
                                        sw.Reset();

                                        if (isNewRow)
                                        {
                                            sw.Start();
                                            context.Cashreports.Add(entity);
                                            saveToDb += sw.ElapsedMilliseconds;
                                            sw.Reset();
                                        }
                                    }
                                }

                                #endregion

                                #region Old Manual code
                                //using (var reader = new StreamReader(new MemoryStream(decryptedString.ToAsciiBytes())))
                                //using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                                //{
                                //    var item = context.Cashreports.Create();

                                //    while (csv.Read())
                                //    {
                                //        //Only process base currency
                                //        if (csv.GetField(4) != "Y")
                                //            continue;

                                //        item.AccountId = csv.GetField(1);

                                //        DateTime.TryParseExact(csv.GetField(2), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime ReportDate);
                                //        item.Date = ReportDate;

                                //        item.BaseCurrency = csv.GetField(3);

                                //        if (csv.GetField(5).EqualsIgnoreCase("Starting Cash"))
                                //            item.StartingCash = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Commissions"))
                                //            item.Commissions = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Internal Transfers"))
                                //            item.InternalTransfers = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Account Transfers"))
                                //            item.AccountTransfers = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Cash Settling MTM"))
                                //            item.CashSettlingMTM = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Net Trades (Sales)"))
                                //            item.NetTradesSales = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Net Trades (Purchase)"))
                                //            item.NetTradesPurchases = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Cash FX Translation Gain/Loss"))
                                //            item.CashFxGainLoss = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Ending Cash"))
                                //            item.EndingCash = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Ending Settled Cash"))
                                //            item.EndingSettledCash = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Deposits"))
                                //            item.Deposits = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Withdrawals"))
                                //            item.Withdrawals = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Debit Card Activity"))
                                //            item.DebitCardActivity = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Broker Interest Paid and Received"))
                                //            item.BrokerInterest = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Transaction Fees"))
                                //            item.TransactionFees = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Other Fees"))
                                //            item.OtherFees = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Change in Insured Bank Deposits"))
                                //            item.ChangeInInsuredBankDeposits = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Dividends"))
                                //            item.Dividends = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Payment In Lieu of Dividends"))
                                //            item.PaymentInLieuOfDividends = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Advisor Fees"))
                                //            item.AdvisorFees = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("Client Fees"))
                                //            item.ClientFees = ParseDecimal(csv.GetField(6));
                                //        else if (csv.GetField(5).EqualsIgnoreCase("GST/HST/PST")
                                //            || csv.GetField(5).EqualsIgnoreCase("Withholding Tax")
                                //            || csv.GetField(5).EqualsIgnoreCase("Insured Deposit Interest")
                                //            || csv.GetField(5).EqualsIgnoreCase("Commission Credits Redemption")
                                //            || csv.GetField(5).EqualsIgnoreCase("CFD Charges")
                                //            || csv.GetField(5).EqualsIgnoreCase("CFD Realized P/L")
                                //            || csv.GetField(5).EqualsIgnoreCase("Bond Interest Paid and Received")
                                //            || csv.GetField(5).EqualsIgnoreCase("Sales Tax")
                                //            || csv.GetField(5).EqualsIgnoreCase("Forex CFD Realized P/L")
                                //            || csv.GetField(5).EqualsIgnoreCase("Debit/Prepaid Card Activity"))
                                //            continue;
                                //        else
                                //            throw new Exception($"Unknown field '{csv.GetField(5)}' in {file.Name} | File={decryptedString}");
                                //    }

                                //    context.Cashreports.Add(item);
                                //    context.SaveChanges();
                                //}
                                #endregion

                            }
                            else if (fileType == FileType.Activity)
                            {
                                //Type,AccountID,ConID,SecurityID,Symbol,BBTicker,BBGlobalID,SecurityDescription,AssetType,Currency,BaseCurrency,TradeDate,TradeTime,SettleDate,TransactionType,Quantity,UnitPrice,GrossAmount,SECFee,Commission,Tax,Net,NetInBase,TradeID,TaxBasisElection,Description,FxRateToBase,ContraPartyName,ClrFirmID,Exchange,MasterAccountID,Van,AwayBrokerCommission,OrderID,ClientReference,TransactionID,
                                //D,F1867187,,,,,,,,USD,USD,2021-06-27,18:00:00,2021-06-27,ADJ,0,0,2.99,0,0,0,2.99,2.99,777058450,,"CLIENT FEE (U4694431, Commission)",1,,,,I533594,,0,,,,

                                logger.Debug($"Processing {file.Name}");
                                using (var reader = new StreamReader(new MemoryStream(decryptedString.ToAsciiBytes())))
                                using (var csv = new CsvReader(reader, readerConf, false))
                                {
                                    #region Process Rows

                                    sw.Start();
                                    var rows = csv.GetRecords<ActivityRecord>();
                                    castRowsToObjects += sw.ElapsedMilliseconds;
                                    sw.Reset();

                                    foreach (ActivityRecord row in rows)
                                    {
                                        sw.Start();
                                        currentRow = row;
                                        var entity = context.Activities.Create();
                                        entity.Type = row.Type;
                                        entity.AccountId = row.AccountID;
                                        entity.AssetType = row.AssetType;
                                        entity.BaseCurrency = row.BaseCurrency;
                                        entity.Currency = row.Currency;
                                        entity.Commission = row.Commission;
                                        entity.Currency = row.Currency;
                                        entity.ConId = row.ConID;
                                        entity.Description = row.Description;
                                        entity.GrossAmount = row.GrossAmount;
                                        entity.Net = row.Net;
                                        entity.NetInBase = row.NetInBase;
                                        entity.Quantity = row.Quantity;
                                        entity.SecFee = row.SECFee;
                                        entity.SecurityId = row.SecurityID;
                                        entity.Symbol = row.Symbol;
                                        entity.Tax = row.Tax;
                                        entity.TIBrokerCode = GetBrokerCodeFromFileName(file.Name, brokerCode.Code);
                                        entity.TradeId = row.TradeID;
                                        entity.TaxBasisElection = row.TaxBasisElection;
                                        entity.TransactionType = row.TransactionType;
                                        entity.UnitPrice = row.UnitPrice;
                                        if (row.TradeDate != null
                                            && DateTime.TryParseExact(row.TradeDate, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime tradeDate))
                                        {
                                            if (false == entity.TradeDate.HasValue) entity.TradeDate = tradeDate;
                                        }
                                        if (row.SettleDate != null
                                            && DateTime.TryParseExact(row.SettleDate, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime settleDate))
                                        {
                                            if (false == entity.SettleDate.HasValue) entity.SettleDate = settleDate;
                                        }
                                        entity.FxRateToBase = row.FXRatetoBase;
                                        entity.ContraPartyName = row.ContraPartyName;
                                        entity.ClrFirmId = row.ClrFirmID;
                                        entity.Exchange = row.Exchange;
                                        entity.MasterAccountId = row.MasterAccountID;
                                        entity.Van = row.Van;
                                        entity.SecurityDescription = row.SecurityDescription;
                                        if (false == string.IsNullOrWhiteSpace(row.TradeTime)
                                            && DateTime.TryParseExact(row.TradeTime, "HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime tradeTime))
                                        {
                                            entity.TradeDate = entity.TradeDate.Value.Add(tradeTime.TimeOfDay);
                                        }
                                        entity.AwayBrokerCommission = row.AwayBrokerCommission;
                                        entity.OrderId = row.OrderID;
                                        entity.BBGlobalId = row.BBGlobalID;
                                        entity.BBTicker = row.BBTicker;
                                        entity.ClientReference = row.ClientReference;
                                        entity.TransactionId = row.TransactionID;
                                        entity.CostBasis = row.CostBasis;
                                        entity.ExecutionId = row.ExcecutionID;
                                        entity.Flag = row.Flag;

                                        //TODO: what is this used for? Where is orderdate?
                                        if (false == string.IsNullOrWhiteSpace(row.OrderTime)
                                            && DateTime.TryParseExact(row.OrderTime, "HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime orderTime))
                                        {
                                            if (entity.OrderDate.HasValue) entity.OrderDate = entity.OrderDate.Value.Add(orderTime.TimeOfDay);
                                        }


                                        //entity.ISOExchangeId = 
                                        //entity.ISOExchangeSymbol=
                                        //entity.C2ExchangeId = ;
                                        //entity.C2PriceMultiplier;
                                        //entity.C2Symbol = ;

                                        processFiles += sw.ElapsedMilliseconds;
                                        sw.Reset();

                                        sw.Start();
                                        context.Activities.Add(entity);
                                        saveToDb += sw.ElapsedMilliseconds;
                                        sw.Reset();
                                    }

                                    #endregion
                                }
                            }

                            //Save only once per file
                            sw.Start();
                            context.SaveChanges();
                            saveToDb += sw.ElapsedMilliseconds;
                            sw.Reset();

                            #endregion

                            #region Store file in plaintext after it is processed successfully

                            if (Properties.Settings.Default.StoreDecrypted)
                            {
                                sw.Start();
                                File.WriteAllText($"{decryptedDirectoryInfo.FullName}\\{file.Name}", decryptedString, Encoding.UTF8);
                                moveFiles += sw.ElapsedMilliseconds;
                                sw.Reset();
                            }

                            #endregion

                            filesToDelete.Add(file.FullName);
                        }
                        catch (System.Data.Entity.Validation.DbEntityValidationException e)
                        {
                            #region Error log

                            StringBuilder sb = new StringBuilder();
                            foreach (var eve in e.EntityValidationErrors)
                            {
                                sb.AppendLine($"Entity of type \"{eve.Entry.Entity.GetType().Name}\" in the state \"{eve.Entry.State}\" has the following validation errors:");
                                foreach (var ve in eve.ValidationErrors)
                                    sb.AppendLine($"- Property: \"{ve.PropertyName}\", Error: \"{ve.ErrorMessage}\"");

                                foreach (var ve in eve.ValidationErrors)
                                    sb.AppendLine($"- Property: \"{ve.PropertyName}\", Value: \"{eve.Entry.CurrentValues.GetValue<object>(ve.PropertyName)}\", Error: \"{ve.ErrorMessage}\"");
                            }
                            logger.Error(e, $"Could not process File (DbEntityValidationException) '{file.Name}': {sb}");

                            #endregion
                        }
                        catch (Exception ex)
                        {
                            logger.Error(ex, $"Could not process File='{file.Name}' -> Row={currentRow}: {ex.GetInnerMostException().Message}.");
                        }
                    }
                }
            }

            //Cleanup so we don't process the files again
            sw.Start();
            foreach (var f in filesToDelete)
            {
                try
                {
                    File.Delete(f);
                }
                catch (Exception ex)
                {
                    logger.Error(ex, $"Could not delete File '{f}': {ex.GetInnerMostException().Message}");
                }
            }
            deleteFiles += sw.ElapsedMilliseconds;
            sw.Reset();

            logger.Info($"Processing {numFiles} {brokerCode.Name} files took: " +
                $"GetFiles={Utils.FormatTimeFromMS(getallFiles)} | " +
                $"DecryptFiles={Utils.FormatTimeFromMS(decryptFiles)} | " +
                $"CastRows={Utils.FormatTimeFromMS(castRowsToObjects)} | " +
                $"ProcessFiles={Utils.FormatTimeFromMS(processFiles)}|" +
                $"SaveToDb={Utils.FormatTimeFromMS(saveToDb)} | " +
                $"MoveFiles={Utils.FormatTimeFromMS(moveFiles)} | " +
                $"DeleteFiles={Utils.FormatTimeFromMS(deleteFiles)}");
        }

        public void MoveActivityFiles()
        {
            //Move Activity File from UnprocessedArchive to RAW.
            //Then Run() will pick them up and process them.

            var regex = new Regex("_Activity_", RegexOptions.IgnoreCase);
            var allUnprocessedActivityFiles = archiveDirectoryInfo
                .EnumerateFiles("*.csv.pgp").Where(f => regex.Match(f.Name).Success);

            int numFiles = allUnprocessedActivityFiles.Count();
            if (numFiles == 0)
            {
                logger.Info($"No {brokerCode.Name} Activity files to process");
                return;
            }
            else
            {
                logger.Info($"Processing {numFiles} Activity files for {brokerCode.Name}");
            }

            Stopwatch sw = Stopwatch.StartNew();
            foreach (var file in allUnprocessedActivityFiles)
            {
                try
                {
                    if (token.IsCancellationRequested)
                        break;

                    logger.Info($"Processing {file.Name}");

                    //Ignore everything except Activity (in case the regex above has a bug)
                    FileType fileType = Utils.GetFileType(file.Name);
                    if (fileType != FileType.Activity)
                    {
                        logger.Warn($"{file.Name} is not supported but it is in the UnprocessedArchive folder and the Regex did not filter it out. Leaving it there.");
                        continue;
                    }
                    else
                    {
                        file.MoveTo($"{activityRawDirectoryInfo.FullName}\\{file.Name}");
                    }
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException e)
                {
					#region Error log

					StringBuilder sb = new StringBuilder();
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        sb.AppendLine($"Entity of type \"{eve.Entry.Entity.GetType().Name}\" in the state \"{eve.Entry.State}\" has the following validation errors:");
                        foreach (var ve in eve.ValidationErrors)
                            sb.AppendLine($"- Property: \"{ve.PropertyName}\", Error: \"{ve.ErrorMessage}\"");

                        foreach (var ve in eve.ValidationErrors)
                            sb.AppendLine($"- Property: \"{ve.PropertyName}\", Value: \"{eve.Entry.CurrentValues.GetValue<object>(ve.PropertyName)}\", Error: \"{ve.ErrorMessage}\"");
                    }
                    logger.Error(e, $"Could not process File '{file.Name}': {sb.ToString()}");

					#endregion
				}
				catch (Exception ex)
                {
                    logger.Error(ex, $"Could not process File '{file.Name}': {ex.GetInnerMostException().Message}");
                }
            }

            logger.Info($"Processing {numFiles} {brokerCode.Name} files took {Utils.FormatTime(sw)}");

        }

        /// <summary>
        /// Hack to remap the BrokerCode if the FTP is the same as C2
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="currentCode"></param>
        /// <returns></returns>
        private int GetBrokerCodeFromFileName(string fileName, int currentCode)
        {
            if (currentCode == BrokerCode.C2 && fileName.StartsWith("F1867187", StringComparison.OrdinalIgnoreCase))
                return BrokerCode.IB_Israel;
            else
                return currentCode;
        }

        private static decimal ParseDecimal(string val)
        {
            if (decimal.TryParse(val, out decimal cash))
                return cash;
            else
                return 0;
        }

        /// <summary>
        /// Expecting input in format U481497_NAV_20190822.csv
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private DateTime GetDateFromFileName(string name)
        {
            var s = Path.GetFileNameWithoutExtension(name).Split('_');
            if (s.Length < 3)
                throw new Exception($"GetDateFromFileName cannot parse date from {name}. We expect format 'U481497_NAV_20190822.csv'");

            if (false == DateTime.TryParseExact(s[2], "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime ParsedDate))
                throw new Exception($"GetDateFromFileName cannot parse date from {s[2]}. We expect format '20190822'");

            return ParsedDate;
        }

        class NAVRecord
        {
            public string Type { get; set; }
            public string AccountID { get; set; }
            public string BaseCurrency { get; set; }
            public decimal Cash { get; set; }
            public decimal CashCollateral { get; set; }
            public decimal Stocks { get; set; }
            public decimal SecuritiesBorrowed { get; set; }
            public decimal SecuritiesLent { get; set; }
            public decimal Options { get; set; }
            public decimal Bonds { get; set; }
            public decimal Commodities { get; set; }
            public decimal Funds { get; set; }
            public decimal Notes { get; set; }
            public decimal Accruals { get; set; }
            public decimal DividendAccruals { get; set; }
            public decimal SoftDollars { get; set; }
            //public decimal InsuredBankDepositSweepProgram { get; set; }
            //public decimal InsuredBankDepositSweepProgramIntAccruals { get; set; }
            public decimal Totals { get; set; }
            public decimal TWR { get; set; }
            public decimal CFDUnrealizedPL { get; set; }
            //public decimal ForexUnrealizedPL { get; set; }
            //public decimal IPOSubscription { get; set; }

        }

        class AccountRecord
        {
            public string Type { get; set; }
            public string AccountID { get; set; }
            public string AccountTitle { get; set; }
            public string Street { get; set; }
            public string Street2 { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string Zip { get; set; }
            public string Country { get; set; }
            public string CustomerType { get; set; }
            public string AccountType { get; set; }
            public string BaseCurrency { get; set; }
            public string MasterAccountID { get; set; }
            public string Van { get; set; }
            public string Capabilities { get; set; }
            public string Alias { get; set; }
            public string PrimaryEmail { get; set; }
            public string DateOpened { get; set; }
            public string DateClosed { get; set; }
            public string DateFunded { get; set; }
            public string AccountRepresentative { get; set; }
        }

        class CashReportRecord
        {
            //Type,AccountID,ReportDate,Currency,BaseSummary,Label,Total,Securities,Futures,IBUKL,
            //D,U7502771,2021-04-19,USD,N,Ending Settled Cash,149044.92069074,149044.92069074,0,0,

            public string Type { get; set; }
            public string AccountID { get; set; }
            public DateTime ReportDate { get; set; }
            public string Currency { get; set; }
            public string BaseSummary { get; set; }
            public string Label { get; set; }
            public decimal Total { get; set; }
            public decimal Securities { get; set; }
            public decimal Futures { get; set; }
            public decimal IBUKL { get; set; }
        }

        class ActivityRecord: LogBase
        {
            /// <summary>
            /// D
            /// </summary>
            public string Type { get; set; }
            /// <summary>
            /// Account number
            /// </summary>
            public string AccountID { get; set; }
            /// <summary>
            /// Internal unique identifier for all asset types. 
            /// Full asset information can be found in the Security File Layout.
            /// Empty for Asset Type CASH.
            /// </summary>
            public string ConID { get; set; }
            /// <summary>
            /// ISIN for US and nonUS securities
            /// </summary>
            public string SecurityID { get; set; }
            /// <summary>
            /// Empty for Asset Type CASH
            /// </summary>
            public string Symbol { get; set; }
            /// <summary>
            /// Bloomberg ticker
            /// </summary>
            [OptionalField]
            public string BBTicker = null;
            /// <summary>
            /// Bloomberg Global ID
            /// </summary>
            [OptionalField]
            public string BBGlobalID = null;
            /// <summary>
            /// Description of the security
            /// </summary>
            [OptionalField]
            public string SecurityDescription = null;

            public string AssetType { get; set; }
            /// <summary>
            /// Currency of the asset
            /// </summary>
            public string Currency { get; set; }
            /// <summary>
            /// Base currency of the account
            /// </summary>
            public string BaseCurrency { get; set; }
            /// <summary>
            /// CCYYMMDD (date format configurable)
            /// </summary>
            public string TradeDate { get; set; }
            /// <summary>
            /// Time of the trade, in HH:mm:ss format
            /// </summary>
            public string TradeTime { get; set; }
            /// <summary>
            /// CCYYMMDD (date format configurable)
            /// </summary>
            public string SettleDate { get; set; }

            [OptionalField]
            public string OrderTime = null;
            /// <summary>
            /// For more information, see Transaction Type.
            /// </summary>
            public string TransactionType { get; set; }
            public decimal? Quantity { get; set; }
            public decimal? UnitPrice { get; set; }
            /// <summary>
            /// -Quantity * Price
            /// Note that this is a general formula and does not consider multipliers or notional price (futures), bond pricing (/100), etc.
            /// </summary>
            public decimal? GrossAmount { get; set; }
            public decimal? SECFee { get; set; }
            /// <summary>
            /// Negative amount indicates commission charged.
            /// Positive amount indicates commission rebate. 
            /// For Forex trades, the commission is reported in base currency.
            /// </summary>
            public decimal? Commission { get; set; }
            public decimal? Tax { get; set; }
            /// <summary>
            /// Total cash received or delivered.
            /// Not included on Forex trades.
            /// For Futures and CFDs, this is the transaction MTM.
            /// </summary>
            public decimal? Net { get; set; }
            /// <summary>
            /// Total cash received or delivered.
            /// For Futures and CFDs, this is the transaction MTM.
            /// </summary>
            public decimal? NetInBase { get; set; }
            /// <summary>
            /// Trade ID, unless cancel or correction, then it is the original Trade ID
            /// </summary>
            public string TradeID { get; set; }
            /// <summary>
            /// For trades, represents the tax basis election
            /// </summary>
            public string TaxBasisElection  { get; set; }
            /// <summary>
            /// Activity description
            /// </summary>
            public string Description { get; set; }
            /// <summary>
            /// Conversion rate from asset currency to base currency.
            /// </summary>
            [OptionalField]
            public decimal? FXRatetoBase = null;
            /// <summary>
            /// For DVP records that represent the contra broker.
            /// </summary>
            [OptionalField] public string ContraPartyName = null;
            /// <summary>
            /// For DVP records, the contra broker’s ID.
            /// </summary>
            [OptionalField] public string ClrFirmID = null;
            /// <summary>
            /// Included for trades.
            /// </summary>
            [OptionalField] public string Exchange = null;
            /// <summary>
            /// The Account ID of the master account.
            /// </summary>
            [OptionalField] public string MasterAccountID = null;
            /// <summary>
            /// Virtual Account Number
            /// </summary>
            [OptionalField] public string Van = null;
            /// <summary>
            /// The away broker's commission charge.
            /// </summary>
            [OptionalField] public decimal? AwayBrokerCommission = null;
            /// <summary>
            /// The IB Order ID.
            /// </summary>
            [OptionalField] public string OrderID = null;
            /// <summary>
            /// Client note entered by cashiering.
            /// </summary>
            [OptionalField] public string ClientReference = null;
            /// <summary>
            /// The IB TransactionID.
            /// </summary>
            [OptionalField] public string TransactionID = null;
            /// <summary>
            /// The IB ExecutionID.
            /// </summary>
            [OptionalField] public string ExcecutionID = null;
            /// <summary>
            /// The basis of an opening trade is the inverse of proceeds plus commission and tax amount. 
            /// For closing trades, the basis is the basis of the opening trade.
            /// </summary>
            [OptionalField] public string CostBasis = null;
            /// <summary>
            /// For more information, see Flag Codes
            /// </summary>
            [OptionalField] public string Flag = null;
        }
    }

}
