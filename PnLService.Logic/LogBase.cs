﻿using System.Text;

namespace PnLService.Logic
{
    /// <summary>
    /// Overrides ToString() and lists all properties and their values
    /// </summary>
    [Serializable]
	public abstract class LogBase
    {
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            System.Reflection.BindingFlags flags = System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance;
            System.Reflection.PropertyInfo[] properties = this.GetType().GetProperties(flags);
			int i = 0;
            foreach (System.Reflection.PropertyInfo property in properties)
            {
				//Ignore Lists & collections & empty properties
				var value = property.GetValue(this, null);
				if (value != null)
				{
					if (i++ == 0)
						sb.Append(property.Name + "=" + value);
					else
						sb.Append(" | " + property.Name + "=" + value);
				}
            }
            return sb.ToString();
        }
    }
}